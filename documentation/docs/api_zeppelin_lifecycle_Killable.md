---
id: zeppelin_lifecycle_Killable
title: Killable
---

<div class="contract-doc"><div class="contract"><h2 class="contract-header"><span class="contract-kind">contract</span> Killable</h2><p class="base-contracts"><span>is</span> <a href="zeppelin_ownership_Ownable.html">Ownable</a></p><div class="source">Source: <a href="https://duarte_1110199@bitbucket.org/duarte_1110199/tmdei-2017-2018-ethdocstore-ethereum/src/master/contracts/zeppelin/lifecycle/Killable.sol" target="_blank">zeppelin/lifecycle/Killable.sol</a></div></div><div class="index"><h2>Index</h2><ul><li><a href="zeppelin_lifecycle_Killable.html#kill">kill</a></li></ul></div><div class="reference"><h2>Reference</h2><div class="functions"><h3>Functions</h3><ul><li><div class="item function"><span id="kill" class="anchor-marker"></span><h4 class="name">kill</h4><div class="body"><code class="signature">function <strong>kill</strong><span>() </span><span>public </span></code><hr/><dl><dt><span class="label-modifiers">Modifiers:</span></dt><dd><a href="zeppelin_ownership_Ownable.html#onlyOwner">onlyOwner </a></dd></dl></div></div></li></ul></div></div></div>
