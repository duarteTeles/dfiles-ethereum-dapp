import Web3 from "web3";

let getWeb3 = new Promise(function(resolve, reject) {
  // Wait for loading completion to avoid race conditions with web3 injection timing.
  window.addEventListener("load", function() {
    var results;
    var web3 = window.web3;
    const ethereum = window.ethereum;

    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof web3 !== "undefined") {
      console.log("Using web3 detected from external source like Metamask");
      // Use Mist/MetaMask's provider.
      if (ethereum) {
        web3 = new Web3(ethereum);
        try {
          // Request account access if needed
          ethereum.enable();
          // Acccounts now exposed
        } catch (error) {
          // User denied account access...
        }
      }
      // Legacy dapp browsers...
      else if (window.web3) {
        web3 = new Web3(web3.currentProvider);
        // Acccounts always exposed
      }

      results = {
        web3: web3
      };

      // console.log('Injected web3 detected.');

      resolve(results);
    } else {
      // Fallback to localhost if no web3 injection.
      console.log(
        "No Metamask Detected!"
        //Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask"
      );
      // let provider = new web3.providers.HttpProvider('http://localhost:7545');
      // web3 = new Web3(provider)

      results = {
        web3: web3
      };

      // console.log('No web3 instance injected, using Local web3.');

      resolve(results);
    }
  });
});

export default getWeb3;
