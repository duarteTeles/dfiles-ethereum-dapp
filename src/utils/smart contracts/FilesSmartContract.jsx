import React, { Component } from "react";
import FilesContract from "../../../build/contracts/Files.json";
import getWeb3 from "../../utils/getWeb3";

const contract = require("truffle-contract");
const filesContract = contract(FilesContract);
import { Form, Button } from "react-bootstrap";

// Import React Table Component
import ReactTableComponent from "../../components/ReactTable";
import ipfs from "../../components/Ipfs";

class FilesSmartContract extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      storageValue: 0,
      accountFrom: null,
      accountBalanceFrom: 0,
      files: [],
      transactionDetails: [],
      web3: null,
      fileBuffer: "",
      isFetchingFiles: true,
      isLoadingTransactionDetails: true,
      isUploadingFiles: false
    };
  }
  getEthPriceInUSD() {
    const url =
      "https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD";
    let etherUSD;
    return fetch(url)
      .then(response => response.json())
      .then(function(response) {
        etherUSD = response.USD;

        return { etherUSD };
      })
      .catch(function(error) {
        etherUSD = 600;
        console.log(error);
        return { etherUSD };
      });
  }

  async uploadFileToBlockchain(accountFrom) {
    const file = {
      name: "run to the hills",
      hash: "0x00000",
      extension: ".mp3",
      metadata: "",
      size: 99,
      timestamp: 11111
    };

    this.setState({ isUploadingFiles: true });
    let filesContractInstance = await filesContract.deployed();
    let transactionHash;
    if (filesContractInstance) {
      try {
        transactionHash = await filesContractInstance.addFile(
          file.name,
          file.hash,
          file.extension,
          file.size,
          file.timestamp,
          {
            from: accountFrom
          }
        );
      } catch (error) {
        console.log(error);
      }

      try {
        await filesContractInstance.addTxHashes(transactionHash.tx, {
          from: accountFrom
        });
      } catch (error) {
        console.log(error);
      }
    }
    this.setState({ isLoadingUploadFile: false });
  }

  async readAllStoredFilesInBlockchain() {
    let filesContractInstance = await filesContract.deployed();
    let fileObject = [];
    try {
      const filesBlockchainArrayLength = (await filesContractInstance.getUserFilesLength()).toNumber();
      if (filesBlockchainArrayLength === 0) {
        console.log("false");
      }

      let newFilesStateArray = this.state.files.slice(); //copy the array

      for (let index = 0; index < filesBlockchainArrayLength; index++) {
        const file = await filesContractInstance.getFileAtIndex(index);

        if (file) {
          fileObject = {
            name: file[0],
            extension: file[1],
            metadata: file[4],
            size: file[2].toNumber(),
            hash: file[3],
            timestamp: file[5].toNumber()
          };
          newFilesStateArray.push(fileObject);
          this.setState({ files: newFilesStateArray });
          this.setState({ isFetchingFiles: false });
        }
      }
    } catch (error) {
      console.log("error: ", error);
    }
  }

  async fetchAllTransactionDetails() {
    let filesContractInstance = await filesContract.deployed();
    let transactionDetailsObject = [];
    let priceEtherUSD = await this.getEthPriceInUSD();

    let txHashesArrayLength = await filesContractInstance.getTxHashesArrayLength();

    for (let index = 0; index < txHashesArrayLength.toNumber(); index++) {
      let txStoredHashes = await filesContractInstance.getTxHashAtIndex(0);
      try {
        let transactionDetails = await this.state.web3.eth.getTransaction(
          txStoredHashes
        );
        transactionDetailsObject.hash = transactionDetails.hash;
        transactionDetailsObject.addressFrom = transactionDetails.from;
        transactionDetailsObject.addressTo = transactionDetails.to;
        transactionDetailsObject.blockNumber = transactionDetails.blockNumber;
        transactionDetailsObject.gas = transactionDetails.gas;
        transactionDetailsObject.gasPrice = parseFloat(
          this.state.web3.utils.fromWei(transactionDetails.gasPrice),
          "ether"
        );
        transactionDetailsObject.gasPriceUSD =
          parseFloat(
            this.state.web3.utils.fromWei(
              transactionDetails.gasPrice.toString(),
              "ether"
            )
          ) * priceEtherUSD.etherUSD;
        transactionDetailsObject.value = transactionDetailsObject.value =
          transactionDetails.value;
        transactionDetailsObject.transactionIndex =
          transactionDetails.transactionIndex;
        const transactionDetailsNewObject = {
          hash: transactionDetailsObject.hash,
          addressTo: transactionDetailsObject.addressTo,
          addressFrom: transactionDetailsObject.addressFrom,
          blockNumber: transactionDetailsObject.blockNumber,
          gas: transactionDetailsObject.gas,
          gasPrice: transactionDetailsObject.gasPrice,
          gasPriceUSD: transactionDetailsObject.gasPriceUSD,
          transactionIndex: transactionDetailsObject.transactionIndex,
          value: transactionDetailsObject.value
        };
        let newTransactionDetailsStateArray = await this.state.transactionDetails.slice(); //copy the array
        newTransactionDetailsStateArray.push(transactionDetailsNewObject);
        if (newTransactionDetailsStateArray) {
          this.setState({
            transactionDetails: newTransactionDetailsStateArray
          }); //set the new state
        }
        this.setState({ isLoadingTransactionDetails: false });
      } catch (error) {
        console.log(error);
      }
    }
  }

  async componentDidMount() {
    // Get network provider and web3 filesContract. See utils/getWeb3 for more info.
    try {
      let results = await getWeb3;
      this.setState({ web3: results.web3 });
      filesContract.setProvider(this.state.web3.currentProvider);
      // Instantiate contract once web3 provided.
      this.updateState();
    } catch (error) {
      console.log("Metamask not found.", error);
    }
  }

  updateState() {
    this.state.web3.eth.getAccounts(async (error, accounts) => {
      if (error) {
        console.log("wtf error", error);
      } else {
        console.log("accounts: ", accounts);
        if (accounts) {
          this.setState({
            accountFrom: accounts[0]
          });
        }
      }

      let accountFromBalance = await this.state.web3.eth.getBalance(
        accounts[0]
      );
      console.log(
        "account balance from account: " + accounts[0] + ": ",
        this.state.web3.utils.fromWei(accountFromBalance, "ether")
      );
      if (accountFromBalance) {
        this.setState({
          accountFromBalance: this.state.web3.utils.fromWei(
            accountFromBalance,
            "ether"
          )
        });
      }
      //this.uploadFileToBlockchain(this.state.accountFrom);
      // this.forceUpdate();
      this.fetchAllTransactionDetails();
      this.readAllStoredFilesInBlockchain();

      //https://michalzalecki.com/how-to-verify-smart-contract-on-etherscan/
    });
  }

  handleSubmit = async event => {
    event.preventDefault();
    //bring in user's metamask account address
    // const accounts = await web3.eth.getAccounts();

    // console.log("Sending from Metamask account: " + accounts[0]);
    // //obtain contract address from storehash.js
    // const ethAddress = await storehash.options.address;
    // this.setState({ ethAddress });

    //save document to IPFS,return its hash#, and set hash# to state
    //https://github.com/ipfs/interface-ipfs-core/blob/master/SPEC/FILES.md#add
    // await ipfs.add(this.state.buffer, (err, ipfsHash) => {
    //   console.log(err, ipfsHash);
    // });
    console.log("IPFS ", ipfs);

    //setState by setting ipfsHash to ipfsHash[0].hash
    // this.setState({ ipfsHash: ipfsHash[0].hash });
    // call Ethereum contract method "sendHash" and .send IPFS hash to etheruem contract
    //return the transaction hash from the ethereum contract
    //see, this https://web3js.readthedocs.io/en/1.0/web3-eth-contract.html#methods-mymethod-send

    // storehash.methods.sendHash(this.state.ipfsHash).send(
    //   {
    //     from: accounts[0]
    //   },
    //   (error, transactionHash) => {
    //     console.log(transactionHash);
    //     this.setState({ transactionHash });
    //   }
    // ); //storehash
    // }); //await ipfs.add
    await ipfs.add(this.state.buffer, (err, ipfsHash) => {
      console.log(err, ipfsHash);
    });

    console.log("events: ", event);
  }; //onSubmit

  captureFile = event => {
    event.stopPropagation();
    event.preventDefault();
    const file = event.target.files[0];
    console.log("file ", event.target.files[0]);
    let reader = new window.FileReader();
    reader.readAsArrayBuffer(file);
    reader.onloadend = () => this.convertToBuffer(reader);
  };
  convertToBuffer = async reader => {
    //file is converted to a buffer for upload to IPFS
    const buffer = await Buffer.from(reader.result);
    //set this buffer -using es6 syntax
    this.setState({ buffer });
    console.log("buffer ", buffer);
  };

  render() {
    /*if (
      (this.state.isFetchingFiles && this.state.isLoadingTransactionDetails) ||
      (this.state.isFetchingFiles &&
        this.state.isLoadingTransactionDetails &&
        this.state.isUploadingFiles)
      ) {
        return <p>Loading, please wait!!!!</p>;
      } else {*/
    return (
      <div className="FilesSmartContract">
        {/* <p>{JSON.stringify(this.state.files)}</p> */}
        {/* <p>{JSON.stringify(this.state.transactionDetails)}</p> */}
        {/* {console.log("details: ", this.state.transactionDetails)} */}
        {/* <h3> Choose file to send to IPFS </h3> */}
        {/* <Form onSubmit={this.handleSubmit}>
          <input
            type="file"
            onChange={this.captureFile}
            accept=".png,.gif"
            placeholder="My Image"
          />
          <Button bsStyle="primary" type="submit">
            {" "}
            Send it
          </Button>
        </Form> */}

        {/* <ReactTableComponent
          files={this.state.files}
          transactionDetails={this.state.transactionDetails}
        /> */}
      </div>
    );
  }
}
// }

export default FilesSmartContract;
