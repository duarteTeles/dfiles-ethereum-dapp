import React, { Component } from "react";
import IPFSFileHashContract from "../../../build/contracts/IPFSFileHash.json";
import getWeb3 from "./utils/getWeb3";
const contract = require("truffle-contract");
const IPFSFileHash = contract(IPFSFileHashContract);

class IPFSFileHashSmartContract extends Component {
  instantiateContract() {
    /*
         * SMART CONTRACT EXAMPLE
         *
         * Normally these functions would be called in the context of a
         * state management library, but for convenience I've placed them here.
         */

    IPFSFileHash.setProvider(this.state.web3.currentProvider);

    // Declaring this for later so we can chain functions on IPFSFileHash.
    var IPFSFileHashInstance;

    // Get accounts.
    this.state.web3.eth.getAccounts((error, accounts) => {
      IPFSFileHash.deployed()
        .then(instance => {
          IPFSFileHashInstance = instance;

          // Stores a given value, 5 by default.
          return IPFSFileHashInstance.set(4, { from: accounts[0] });
        })
        .then(result => {
          // Get the value from the contract to prove it worked.
          return IPFSFileHashInstance.get.call(accounts[0]);
        })
        .then(result => {
          // Update state with the result.
          return this.setState({ storageValue: result.c[0] });
        });
    });
  }

  render() {
    return (
      <div>
        gg
        <div>gg</div>
      </div>
    );
  }
}

export default IPFSFileHashSmartContract;
