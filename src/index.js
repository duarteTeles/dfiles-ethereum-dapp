import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';



/* import App styles */
import "./styles/scss/global/app.scss";

// import 'bootstrap/dist/css/bootstrap.css';
// import './styles/css/bootstrap.min.css';
// import './styles/css/mdb.css';
// import './styles/css/style.css';




ReactDOM.render(
  <App/> ,
  document.getElementById('root')
);