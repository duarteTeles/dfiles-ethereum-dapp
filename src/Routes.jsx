import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
  browserHistory,
  withRouter
} from "react-router-dom";
import Home from "./pages/home/Home.jsx";
import UploadFiles from "./pages/files/upload files/UploadFiles";
import UploadFiles2 from "./pages/files/upload files/UploadFiles2";
import MyFiles from "./pages/files/my files/MyFiles";
import Sharing from "./pages/files/sharing/Sharing";
import MyAccount from "./pages/my account/MyAccount";
import Login from "./auth/Login";
import Register from "./auth/Register";
import MyAccountSettings from "./pages/my account/MyAccountSettings";
import MyAccountPrivacyAndGDPR from "./pages/my account/MyAccountPrivacyAndGDPR";
import NotFound from "./pages/error/404/NotFound";
import PrivacyPolicy from "./pages/privacy/PrivacyPolicy";

class Routes extends React.Component {
  render() {
    return (
      // //////////////////////////////////////////////////////////
      // https://scotch.io/tutorials/routing-react-apps-the-complete-guide#more-routes
      // https://www.sitepoint.com/react-router-v4-complete-guide/
      <Router>
        {/* <AuthButton /> */}
        <Switch>
          <Route exact path="/" component={Home} history={browserHistory} />
          <Route
            exact
            path="/files/upload-files"
            component={UploadFiles}
            history={browserHistory}
          />
          <Route
            exact
            path="/files/my-files"
            component={MyFiles} // component={MyFiles}
            // render={props => <MyFiles {...props} gg={true} />}
            history={browserHistory}
          />
          <Route
            exact
            path="/files/sharing"
            component={Sharing}
            history={browserHistory}
          />
          <Route
            exact
            path="/auth/account"
            component={MyAccount}
            history={browserHistory}
          />
          <Route
            exact
            path="/auth/account/settings"
            component={MyAccountSettings}
            history={browserHistory}
          />
          <Route
            exact
            path="/auth/account/settings/privacy"
            component={MyAccountPrivacyAndGDPR}
            history={browserHistory}
          />
          <Route
            exact
            path="/auth/login"
            component={Login}
            history={browserHistory}
          />
          <Route
            exact
            path="/auth/register"
            component={Register}
            history={browserHistory}
          />
          <Route
            exact
            path="/privacy/privacy-policy"
            component={PrivacyPolicy}
            history={browserHistory}
          />
          {/* <Route
            exact
            path="/test/uploadFiles"
            component={UploadFiles2}
            history={browserHistory}
          /> */}
          <NotFound />
        </Switch>
      </Router>
    );
  }
}

export default Routes;
