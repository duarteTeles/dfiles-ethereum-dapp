import React from "react";
import { render } from "react-dom";
import { withFormik } from "formik";
import * as Yup from "yup";
import NavbarItems from "../components/Navbar";
import axios from "axios";
import cookie from "react-cookies";

class MyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    // const {
    //   values,
    //   touched,
    //   errors,
    //   isSubmitting,
    //   handleChange,
    //   handleBlur,
    //   handleSubmit,
    //   handleReset,
    //   dirty
    // } = props;
  }
  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <h1>Login </h1>
        <div className="form-group">
          <label
            htmlFor="username"
            style={{ display: "block", marginTop: ".5rem" }}
          >
            Username
          </label>
          <input
            id="username"
            placeholder="Enter your username"
            type="text"
            value={this.props.values.username}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.username &&
            this.props.touched.username && (
              <div style={{ color: "red", marginTop: ".5rem" }}>
                {this.props.errors.username}
              </div>
            )}
        </div>
        <div className="form-group">
          <label
            htmlFor="password"
            style={{ display: "block", marginTop: ".5rem" }}
          >
            Password
          </label>
          <input
            id="password"
            placeholder="Enter your password"
            type="password"
            value={this.props.values.password}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.password &&
            this.props.touched.password && (
              <div style={{ color: "red", marginTop: ".5rem" }}>
                {this.props.errors.password}
              </div>
            )}
        </div>
        <button
          type="button"
          className="outline btn btn-default"
          onClick={this.props.handleReset}
          disabled={!this.props.dirty || this.props.isSubmitting}
        >
          Reset
        </button>
        <button
          type="submit"
          className={"btn btn-primary"}
          disabled={this.props.isSubmitting}
        >
          Submit
        </button>
      </form>
    );
  }
}

const MyLoginEnhancedForm = withFormik({
  mapPropsToValues: props => ({
    username: "",
    password: ""
  }),
  validationSchema: Yup.object().shape({
    username: Yup.string()
      .matches(/^.[a-zA-Z0-9_]+$/, {
        message: "Username must only contain letters or numbers",
        excludeEmptyString: true
      })
      .required("Username is required"),
    password: Yup.string().required("Password is required")
  }),

  handleSubmit: (values, { setSubmitting, props }) => {
    event.preventDefault();
    console.log("props ", props);

    // const username = cookie.load("userData").username;
    // setTimeout(() => {
    //   alert(JSON.stringify(values, null, 2));
    //   setSubmitting(false);
    // }, 1000);

    // cookie.save("userId", "1234", {
    //   path: "/",
    //   //expires,
    //   maxAge: 1000,
    //   domain: "http://localhost:3005",
    //   secure: false
    //   // httpOnly: true
    // });

    axios
      .get(
        `http://localhost:3005/users/login?username=${
          values.username
        }&password=${values.password}`
      )
      .then(function(response) {
        let userData = {
          username: response.data.username,
          email: response.data.email,
          ethereumAccountAddress: response.data.ethereumAccountAddress,
          privateKey: response.data.privateKey,
          publicKey: response.data.publicKey
          // firstName: response.data.firstName,
          // lastName: response.data.lastName
        };
        // alert(JSON.stringify(response.data));

        //Perform action based on response

        if (cookie.load("userData") !== undefined) {
          cookie.remove("userData", { path: "/" });
        }
        console.log("userData ", userData);

        cookie.save("userData", JSON.stringify(userData), {
          path: "/",
          // expires,
          maxAge: 7200,
          secure: false
        });
        props.history.push("/files/upload-files");
      })
      .catch(function(error) {
        console.log("error: ", error);
        if (!error.response) {
          alert("Network error, try again");
          console.log("error.response ", error.response);
        } else {
          if (error.response) {
            if (error.response.status === 404) {
              alert("Invalid username or password");
            } else if (error.response.status === 500) {
              alert("A serious error has occurred");
            }
            console.log("Error ", error.response);
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
          }
        }
      });
    setSubmitting(false);
  },

  displayName: "LoginForm" // helps with React DevTools
})(MyForm);
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // username: cookie.load("onboarded")
    };
  }

  render() {
    return (
      <div
        className="container-fluid"
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        {/* <h1>Login</h1> */}
        <NavbarItems />
        <MyLoginEnhancedForm {...this.props} />
      </div>
    );

    // TODO: Finish Login Form: https://mdbootstrap.com/react/components/forms/
  }
}

export default Login;
