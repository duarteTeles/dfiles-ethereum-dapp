import React from "react";
import { render } from "react-dom";
import { withFormik, Field } from "formik";
import * as Yup from "yup";
import NavbarItems from "../components/Navbar";
import axios from "axios";
import getWeb3 from "../utils/getWeb3";
import Select from "react-select";
import keypair from "keypair";
import DatePicker from "react-datepicker";
import moment from "moment";
import classNames from "classnames";

import "react-datepicker/dist/react-datepicker.css";
// import { randomValueHex } from "../utils/UtilFunctions";

let keyPair = keypair();
let privateKey = keyPair.private;
let publicKey = keyPair.public;

const serverUrl = "http://localhost:3005";

//Understand React global state: https://github.com/schiehll/react-globally

// Input feedback
const InputFeedback = ({ error }) =>
  error ? <div className={classNames("input-feedback")}>{error}</div> : null;

// Checkbox input
const Checkbox = ({
  field: { name, value, onChange, onBlur },
  form: { errors, touched, setFieldValue },
  id,
  label,
  className,
  ...props
}) => {
  return (
    <div>
      <input
        name={name}
        id={id}
        type="checkbox"
        value={value}
        checked={value}
        onChange={onChange}
        onBlur={onBlur}
        className={classNames("radio-button")}
      />
      <label htmlFor={id}>{label}</label>
      <div style={{ color: "red", marginTop: ".5rem" }}>
        {touched[name] && <InputFeedback error={errors[name]} />}
      </div>
    </div>
  );
};

class MyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      privateKey: privateKey,
      publicKey: publicKey,
      startDate: moment()
    };
    this.handleGenerateNewKeyPair = this.handleGenerateNewKeyPair.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date) {
    this.setState({
      startDate: date
    });
  }

  handleGenerateNewKeyPair = event => {
    keyPair = keypair();
    privateKey = keyPair.private;
    publicKey = keyPair.public;
    this.setState({ privateKey: privateKey, publicKey: publicKey });
  };

  render() {
    const { value, error, touched, label, className, children } = this.props;

    const classes = classNames(
      "input-field",
      {
        "is-success": value || (!error && touched), // handle prefilled or user-filled
        "is-error": !!error && touched
      },
      className
    );
    return (
      <form onSubmit={this.props.handleSubmit}>
        <h1>Register </h1>

        <div className="form-group">
          <label
            htmlFor="username"
            style={{ display: "block", marginTop: ".5rem" }}
          >
            <strong>Username</strong>
          </label>
          <input
            id="username"
            autoComplete="username"
            placeholder="Enter your username"
            type="text"
            value={this.props.values.username}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.username && this.props.touched.username && (
            <div style={{ color: "red", marginTop: ".5rem" }}>
              {this.props.errors.username}
            </div>
          )}
        </div>
        <div className="form-group">
          <label
            htmlFor="email"
            style={{ display: "block", marginTop: ".5rem" }}
          >
            <strong>Email</strong>
          </label>
          <input
            id="email"
            autoComplete="email"
            placeholder="Enter your email"
            type="email"
            value={this.props.values.email}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.email && this.props.touched.email && (
            <div style={{ color: "red", marginTop: ".5rem" }}>
              {this.props.errors.email}
            </div>
          )}
        </div>
        <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <label
                htmlFor="new-password"
                style={{ display: "block", marginTop: ".5rem" }}
              >
                <strong>Password</strong>
              </label>
              <input
                id="password"
                autoComplete="password"
                placeholder="Enter your password"
                type="password"
                value={this.props.values.password}
                onChange={this.props.handleChange}
                onBlur={this.props.handleBlur}
                className="form-control"
              />
              {this.props.errors.password && this.props.touched.password && (
                <div style={{ color: "red", marginTop: ".5rem" }}>
                  {this.props.errors.password}
                </div>
              )}
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label
                htmlFor="retypePassword"
                style={{ display: "block", marginTop: ".5rem" }}
              >
                <strong>Retype Password</strong>
              </label>
              <input
                id="retypePassword"
                placeholder="Retype your password"
                type="password"
                value={this.props.values.retypePassword}
                onChange={this.props.handleChange}
                onBlur={this.props.handleBlur}
                className="form-control"
              />
              {this.props.errors.retypePassword &&
                this.props.touched.retypePassword && (
                  <div style={{ color: "red", marginTop: ".5rem" }}>
                    {this.props.errors.retypePassword}
                  </div>
                )}
            </div>
          </div>
        </div>

        {/* <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <label
                htmlFor="firstName"
                style={{ display: "block", marginTop: ".5rem" }}
              >
                <strong>First Name</strong>
              </label>
              <input
                id="firstName"
                autoComplete="given-name"
                placeholder="Enter your first name"
                type="text"
                value={this.props.values.firstName}
                onChange={this.props.handleChange}
                onBlur={this.props.handleBlur}
                className="form-control"
              />
              {this.props.errors.firstName &&
                this.props.touched.firstName && (
                  <div style={{ color: "red", marginTop: ".5rem" }}>
                    {this.props.errors.firstName}
                  </div>
                )}
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label
                htmlFor="lastName"
                style={{ display: "block", marginTop: ".5rem" }}
              >
                <strong>Last Name</strong>
              </label>
              <input
                id="lastName"
                placeholder="Enter your last name"
                autoComplete="family-name"
                type="text"
                value={this.props.values.lastName}
                onChange={this.props.handleChange}
                onBlur={this.props.handleBlur}
                className="form-control"
              />
              {this.props.errors.lastName &&
                this.props.touched.lastName && (
                  <div style={{ color: "red", marginTop: ".5rem" }}>
                    {this.props.errors.lastName}
                  </div>
                )}
            </div>
          </div>
        </div> */}

        {/* <div className="form-group">
        <label
          htmlFor="lastName"
          style={{ display: "block", marginTop: ".5rem" }}
        >
          <strong>Ethereum Account Address</strong>
        </label>
        <input
          id="lastName"
          placeholder="Enter your last name"
          type="text"
          value={values.lastName}
          onChange={handleChange}
          onBlur={handleBlur}
          className="form-control"
        />
        {errors.lastName &&
          touched.lastName && (
            <div style={{ color: "red", marginTop: ".5rem" }}>
              {errors.lastName}
            </div>
          )}
      </div> */}

        <div className="form-group">
          <MySelect
            value={this.props.values.ethereumAccountAddress}
            onChange={this.props.setFieldValue}
            onBlur={this.props.setFieldTouched}
            error={this.props.errors.ethereumAccountAddress}
            touched={this.props.touched.ethereumAccountAddress}
            // placeholder= {this.state.web3 ? "Select Ethereum Account Address" : "Metamask not found. Please install the Metamask extension and create an Ethereum Wallet to continue" }
          />
        </div>

        <div className="form-group">
          <label
            htmlFor="privateKey"
            style={{ display: "block", marginTop: ".5rem" }}
          >
            <strong>User Private Key</strong>
          </label>
          <div className="input-group">
            <input
              id="privateKey"
              readOnly="readonly"
              value={this.state.privateKey}
              type="text"
              className="form-control"
            />

            {/* <button
              type="button"
              className="outline btn btn-elegant btn-md"
              onClick={this.handleGenerateNewKeyPair}
              disabled={this.props.isSubmitting}
            >
              Generate New Key
            </button> */}
          </div>
        </div>

        <div className="form-group">
          <label
            htmlFor="publicKey"
            style={{ display: "block", marginTop: ".5rem" }}
          >
            <strong>User Public Key</strong>
          </label>
          <div className="input-group">
            <input
              id="publicKey"
              readOnly="readonly"
              value={this.state.publicKey}
              type="text"
              className="form-control"
            />
          </div>
        </div>

        <div className="form-group">
          <button
            type="button"
            // style={{ marginTop: ".5rem" }}
            className="outline btn btn-elegant btn-md"
            onClick={this.handleGenerateNewKeyPair}
            disabled={this.props.isSubmitting}
          >
            Generate New Key Pair
          </button>
        </div>

        <div className="form-group">
          {/* <label
            htmlFor="email"
            style={{ display: "block", marginTop: ".5rem" }}
          >
            <strong>Email</strong>
          </label> */}
          {/* <input
            className="form-control form-check-input"
            type="checkbox"
            value={this.props.values.gdprAge}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            id="gdprAge"
          />
          <label className="form-check-label" htmlFor="gdprAge">
            I am 16 years old or older
          </label>
          {this.props.errors.gdprAge &&
            this.props.touched.gdprAge && (
              <div style={{ color: "red", marginTop: ".5rem" }}>
                {this.props.errors.gdprAge}
              </div>
            )} */}

          <Field
            component={Checkbox}
            name="gdprAge"
            id="gdprAge"
            label="I am 16 years old or over"
          />
          {/* <input
            id="email"
            autoComplete="email"
            placeholder="Enter your email"
            type="email"
            value={this.props.values.email}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.email &&
            this.props.touched.email && (
              <div style={{ color: "red", marginTop: ".5rem" }}>
                {this.props.errors.email}
              </div>
            )} */}
        </div>

        {/* {errors.lastName &&
          touched.lastName && (
            <div style={{ color: "red", marginTop: ".5rem" }}>
              {errors.lastName}
            </div>
          )} */}

        <button
          type="button"
          className="outline btn btn-default"
          onClick={this.props.handleReset}
          disabled={!this.props.dirty || this.props.isSubmitting}
        >
          Reset
        </button>
        <button
          type="submit"
          className={"btn btn-primary"}
          disabled={this.props.isSubmitting}
        >
          Submit
        </button>
      </form>
    );
  }
}

const MyLoginEnhancedForm = withFormik({
  mapPropsToValues: props => ({
    username: "",
    email: "",
    password: "",
    retypePassword: "",
    // firstName: "",
    // lastName: "",
    ethereumAccountAddress: [],
    gdprAge: false
  }),
  validationSchema: Yup.object().shape({
    username: Yup.string()
      .matches(/^.[a-zA-Z0-9_]+$/, {
        message: "Username must only contain letters or numbers",
        excludeEmptyString: true
      })
      .required("Username is required"),
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required"),
    password: Yup.string().required("Password is required"),
    retypePassword: Yup.string()
      .oneOf([Yup.ref("password")], "Passwords do not match")
      .required("Password retype is required"),
    gdprAge: Yup.bool().oneOf(
      [true],
      "Sorry, you have to be 16 years or older."
    )

    // firstName: Yup.string()
    //   .min(2, "First name must be 2 characters or longer")
    //   .max(40, "First name too long")
    //   .matches(/^[a-zA-Z_\-]+$/, {
    //     message: "Invalid first name",
    //     excludeEmptyString: true
    //   })
    //   .required("First name is required"),
    // lastName: Yup.string()
    //   .min(2, "Last name must be 2 characters or longer")
    //   .max(40, "Last name too long")
    //   .matches(/^[a-zA-Z_\-]+$/, {
    //     message: "Invalid last name",
    //     excludeEmptyString: true
    //   })
    //   .required("Last name is required")
    // ethereumAccountAddress: Yup.array()
    //   .min(1, "Please select an Ethereum Account Address")
    //   .of(
    //     Yup.object().shape({
    //       label: Yup.string().required(),
    //       value: Yup.string().required()
    //     })
    //   )
  }),

  handleSubmit: (values, { setSubmitting }) => {
    // setTimeout(() => {
    //   alert(JSON.stringify(values, null, 2));
    //   setSubmitting(false);
    // }, 1000);

    axios
      .post(`${serverUrl}/users/`, {
        username: values.username,
        // firstName: values.firstName,
        // lastName: values.lastName,
        email: values.email,
        password: values.password,
        ethereumAccountAddress: values.ethereumAccountAddress.value.toLowerCase(),
        privateKey: privateKey,
        publicKey: publicKey
      })

      .then(function(response) {
        console.log(response.data);
        // alert(JSON.stringify(response.data));
        alert(
          `User with username ${response.data.username} registered successfully`
        );
        //Perform action based on response
      })
      .catch(function(error) {
        if (!error.response) {
          alert("Network error, try again");
        } else {
          if (error.response) {
            if (error.response.status === 500) {
              alert("A serious error has occurred while registering user");
            }
            console.log("Error ", error.response);
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
          }
        }
      });
    setSubmitting(false);
  },

  displayName: "LoginForm" // helps with React DevTools
})(MyForm);

const ethereumWalletAccountsList = [
  // { value: "Food", label: "Food" },
  // { value: "Being Fabulous", label: "Being Fabulous" },
  // { value: "Ken Wheeler", label: "Ken Wheeler" },
  // { value: "ReasonML", label: "ReasonML" },
  // { value: "Unicorns", label: "Unicorns" },
  // { value: "Kittens", label: "Kittens" }
];

class MySelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      web3: null,
      ethereumAccounts: []
    };
  }

  async componentDidMount() {
    try {
      let results = await getWeb3;
      this.setState({ web3: results.web3 });
    } catch (error) {
      console.log("Metamask not found.", error);
      this.setState({ web3: null });
    }

    if (this.state.web3 !== undefined) {
      this.state.web3.eth.getAccounts(async (error, accounts) => {
        if (error) {
          console.log("wtf error 2", error);
        } else {
          for (let i = 0; i < accounts.length; i++) {
            ethereumWalletAccountsList.push({
              value: accounts[i],
              label: accounts[i].toString()
            });
          }

          console.log("accounts: ", accounts);
          this.setState({ ethereumAccounts: accounts });
          console.log("Ethereum Accounts State ", this.state.ethereumAccounts);
        }
      });
    }
  }

  handleChange = value => {
    // this is going to call setFieldValue and manually update values.
    this.props.onChange("ethereumAccountAddress", value);
  };

  handleBlur = () => {
    // this is going to call setFieldTouched and manually update touched.
    this.props.onBlur("ethereumAccountAddress", true);
  };

  // TODO: Populate HTML Select with Ethereum Accounts. Then, generate a private key (web3JS API). Finally, register the user.
  render() {
    return (
      <div style={{ margin: "1rem 0" }}>
        <label htmlFor="color">
          <strong>Ethereum Account Address </strong>{" "}
        </label>
        <Select
          id="color"
          options={
            ethereumWalletAccountsList.length > 0
              ? ethereumWalletAccountsList
              : []
          }
          autoFocus={true}
          multi={false}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={this.props.value}
          isDisabled={this.state.web3 ? false : true}
          placeholder={
            this.state.web3
              ? "Select Ethereum Account Address"
              : "Metamask not found. Please install the Metamask extension and create an Ethereum Wallet to continue"
          }
          noOptionsMessage={() => "No Ethereum accounts found."}
        />
        {!!this.props.error && this.props.touched && (
          <div style={{ color: "red", marginTop: ".5rem" }}>
            {this.props.error}
          </div>
        )}
      </div>
    );
  }
}

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div
        className="container-fluid"
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        {/* <h1>Login</h1> */}
        <NavbarItems />
        <MyLoginEnhancedForm />
      </div>
    );
  }
}

export default Register;
