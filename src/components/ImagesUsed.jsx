// import LogoImage from "../pages/home/footer/footer items/LogoImage";

const ImagesUsed = {
  header: {
    src: require("../img/EthDocStore Small.png")
    // comments: require('./profile/comments.png'),
  },
  main: {
    sectionOne_DecentralizedTechnologies: {
      ipfs: {
        src: require("../img/ipfs_icon.png"),
        id: "ipfs-icon",
        alt: "Footer Logo",
        title: "Footer Logo",
        HTMLName: "IPFS",
        height: 50,
        width: 50,
        class: ""
      },
      bigchainDB: {
        src: require("../img/bigchainDB.jpeg"),
        id: "bigchainDB-icon",
        alt: "Bigchain DB",
        title: "Bigchain DB",
        HTMLName: "Bigchain DB",
        height: "",
        width: "80%",
        class: ""
      }
    },
    sectionTwo_GDPRCompliance: {
      gdpr: {
        src: require("../img/gdpr1.jpg"),
        id: "gdprImage",
        alt: "GDPR Compliance",
        title: "GDPR Compliance",
        HTMLName: "GDPR Compliance",
        height: "",
        width: "",
        class: "img-fluid"
      }
    },
    sectionThree_Features: {
      EDSCoin: {
        src: require("../img/EDS Coin.svg"),
        id: "feature_edsCoin",
        alt: "EDS Coin",
        title: "EDS Coin",
        HTMLName: "EDS Coin",
        height: "150",
        width: "",
        class: ""
      },
      Decentralized: {
        src: require("../img/decentralized.svg"),
        id: "feature_decentralized",
        alt: "Decentralized",
        title: "Decentralized",
        HTMLName: "Decentralized",
        height: "80",
        width: "",
        class: ""
      },
      DocumentImmutability: {
        src: require("../img/documentImmutabilityIcon.svg"),
        id: "feature_documentImmutability",
        alt: "Document Immutability Icon",
        title: "Document Immutability Icon",
        HTMLName: "Document Immutability Icon",
        height: "70",
        width: "",
        class: ""
      },
      TrustlessAndSecureDocumentUploadingAndSharing: {
        src: require("../img/eds-features-Trustless and Secure Document Uploading & Sharing.svg"),
        id: "feature_trustlessAndSecureDocumentUploadingAndSharing",
        alt: "Trustless and Secure Document Uploading & Sharing",
        title: "Trustless and Secure Document Uploading & Sharing",
        HTMLName: "Trustless and Secure Document Uploading & Sharing",
        height: "150",
        width: "",
        class: ""
      },
    }
  },
  footer:{
    LogoImage: {
        src: require("../img/EthDocStore Logo.svg"),
        id: "footerLogoImage",
        alt: "Footer Logo",
        title: "Footer Logo",
        HTMLName: "Footer Logo",
        height: 100,
        width: "",
        class: "mx-auto d-block"
  }, 
}
};

export default ImagesUsed;
