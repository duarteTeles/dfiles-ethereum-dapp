import React from "react";
import PropTypes from "prop-types";

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";
import Button from "./Button";

const columns = [
  {
    Header: "Ethereum Transaction Receipt",
    columns: [
      {
        Header: "Tx Number",
        id: "txNumber",
        accessor: "txNumber"
      },
      {
        Header: "Tx Hash",
        id: "txHash",
        accessor: "txHash"
      },
      {
        Header: "Address From",
        id: "addressFrom",
        accessor: "addressFrom"
      },
      {
        Header: "Gas Price",
        id: "gasPrice",
        accessor: "gasPrice"
      },
      {
        Header: "IPFS File Hash",
        id: "fileHash",
        accessor: "fileHash"
      },
      {
        Header: "Preview",
        id: "filePreview",
        accessor: "filePreview",
        filterable: false
      },
      {
        Header: "Download",
        id: "fileDownload",
        accessor: "fileDownload",
        filterable: false
      }
    ]
  }
];

export default class ReactTableComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // data: [
      //   // {
      //   //   //   // txNumber: 0
      //   //   //   // txHash: "",
      //   //   //   // addressFrom:"",
      //   //   //   // addressTo:"",
      //   //   //   // blockNumber:0,
      //   //   //   // value:0,
      //   //   //   // gasPrice:0,
      //   //   //   // gasPriceUSD:0,
      //   //   //   }]
      //   //   // txNumber: 0,
      //   //   // txHash: "",
      //   //   // addressFrom: "",
      //   //   // addressTo: "",
      //   //   // blockNumber: 0,
      //   //   // gasLimit: 0,
      //   //   // gas: 0,
      //   //   // gasPrice: 0,
      //   //   // gasPriceUSD: 0
      //   // }
      // ]
      data: []
    };
  }

  componentDidUpdate(prevProps) {
    //TODO: FIX THIS INDEX BUG

    let filesAndTransactionsObject = {};
    let tempFilesAndTransactionsObject = this.state.data.slice(); //copy the array
    let filesObject = {};

    if (
      this.props.transactionDetails.length !==
      prevProps.transactionDetails.length
    ) {
      // let tempSet = [];
      // this.setState({ data: filesAndTransactionsObject });

      this.props.transactionDetails.map((item, index) => {
        // for (
        //   let index = 0;
        //   index < this.props.transactionDetails.length;
        //   index++
        // ) {
        filesAndTransactionsObject.txNumber = index + 1;
        filesAndTransactionsObject.txHash = this.props.transactionDetails[
          index
        ].hash;
        filesAndTransactionsObject.addressFrom = this.props.transactionDetails[
          index
        ].addressFrom;
        filesAndTransactionsObject.addressTo = this.props.transactionDetails[
          index
        ].addressTo;
        filesAndTransactionsObject.blockNumber = this.props.transactionDetails[
          index
        ].blockNumber;
        filesAndTransactionsObject.value = this.props.transactionDetails[
          index
        ].value;
        // filesAndTransactionsObject.gasLimit = this.props.transactionDetails[
        //   index
        // ].gasLimit;
        filesAndTransactionsObject.gas = this.props.transactionDetails[
          index
        ].gas;
        filesAndTransactionsObject.gasPrice = this.props.transactionDetails[
          index
        ].gasPrice;
        filesAndTransactionsObject.gasPriceUSD = this.props.transactionDetails[
          index
        ].gasPriceUSD;

        // // actualTxFee: 11

        // FILES
        filesAndTransactionsObject.fileName = this.props.files[index].name;
        filesAndTransactionsObject.fileExtension = this.props.files[
          index
        ].extension;
        filesAndTransactionsObject.fileMetadata = this.props.files[
          index
        ].metadata;
        filesAndTransactionsObject.fileSize = this.props.files[index].size;
        filesAndTransactionsObject.fileHash = this.props.files[index].hash;
        filesAndTransactionsObject.fileTimestamp = this.props.files[
          index
        ].timestamp;
        filesAndTransactionsObject.filePreview = (
          <Button
            color="indigo"
            size="sm"
            className="mr-4 d-flex justify-content-left"
          >
            <i className="fas fa-upload" />
          </Button>
        );
        filesAndTransactionsObject.fileDownload = (
          <Button
            color="indigo"
            size="sm"
            className="mr-4 d-flex justify-content-left"
          >
            <i className="fas fa-upload" />
          </Button>
        );
      });

      tempFilesAndTransactionsObject.push(filesAndTransactionsObject);

      console.log("tempObject ", tempFilesAndTransactionsObject);
      this.setState({ data: tempFilesAndTransactionsObject });
    }
  }

  render() {
    const { data } = this.state;

    return (
      <div className="container-fluid">
        <ReactTable
          data={data}
          filterable
          columns={columns}
          defaultPageSize={10}
          className="-striped -highlight"
          style={
            { height: "600px" } // This will force the table body to overflow and scroll, since there is not enough room
          }
          SubComponent={row => {
            // a SubComponent just for the final detail
            const columns = [
              {
                Header: "Transaction Info",
                accessor: "txInfo",
                Cell: ci => {
                  return `${ci.value}`;
                },
                style: {
                  backgroundColor: "#DDD",
                  textAlign: "center",
                  fontWeight: "bold"
                }
              },
              {
                Header: "Value",
                accessor: "value",
                style: { textAlign: "center" }
              }
            ];
            const rowData = Object.keys(row.original).map(key => {
              return {
                txInfo: key,
                value: row.original[key].toString()
              };
            });

            return (
              <div style={{ padding: "10px" }}>
                <ReactTable
                  data={rowData}
                  columns={columns}
                  pageSize={rowData.length}
                  showPagination={false}
                />
              </div>
            );
          }}
        />
      </div>
    );
  }
}
