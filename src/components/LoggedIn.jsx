import React from "react";
import PropTypes from "prop-types";
import cookie from "react-cookies";
import { Link } from "react-router-dom";

const propTypes = {};

const defaultProps = {};

export default class LoggedIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = { userLoggedIn: false };
  }

  isUserLoggedIn() {
    if (cookie.load("userData") === undefined) {
      return (
        <h4>
          Please{" "}
          <Link to={`/auth/login/`}>
            {" "}
            <strong>Login </strong>
          </Link>
          to view this page.
        </h4>
      );
    }
  }

  render() {
    return <div>{this.isUserLoggedIn()}</div>;
  }
}

LoggedIn.propTypes = propTypes;
LoggedIn.defaultProps = defaultProps;
