import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

// import Button styles
import './button.scss';


class Button extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    // this.onClick = this.onClick.bind(this);
  }

  //   handleClick(e) {
  //     // Get Cursor Position
  //     let cursorPos = {
  //       top: e.clientY,
  //       left: e.clientX,
  //       time: Date.now()
  //     };
  //     this.setState({ cursorPos: cursorPos });
  //   }

  //   onClick(e) {
  //     if (this.props.disabled) {
  //       e.preventDefault();
  //       return;
  //     }

  //     if (this.props.onClick) {
  //       this.props.onClick(e);
  //     }
  //   }

  render() {
    const {
      color,
      size,
      block,
      active,
      disabled,
      rounded,
      gradient,
      outline,
      floating,
      fixed,
      link,
      children,
      flat,
      className,
      id, 
     
    } = this.props;
    // gradient ? `${gradient}-gradient` : `btn${outline ? '-outline' : ''}-${color}`,

    const classes = classNames(
      "btn",
      `btn-${color}`,
      size ? `btn-${size}` : false,
      block ? "btn-block" : false,
      link ? "btn-link" : false,
      rounded ? "btn-rounded" : false,
      gradient ? `${gradient}-gradient` : false,
      outline ? `btn-outline-  ${color} waves-effect` : false,
      floating ? "floating" : false,
      fixed ? "fixed-action-btn" : false,
      flat ? "btn-flat" : false,
      active ? "active" : false,
      disabled ? "disabled" : false,
      children,
      className,
      floating ? "btn-floating" : "btn",
      flat ? "btn-flat" : false,
      rounded ? "btn-rounded" : false,
      className
    );

    return (
      <button  type="button" id={id} className={classes}>
        {this.props.children}
      </button>
    );
  }
}

Button.defaultProps = {
  color: "elegant",
  size: "md"
};

Button.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.string,
  block: PropTypes.bool,
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  rounded: PropTypes.bool,
  gradient: PropTypes.string,
  outline: PropTypes.bool,
  floating: PropTypes.bool,
  fixed: PropTypes.bool,
  flat: PropTypes.bool,
  link: PropTypes.bool,
  //   onClick: PropTypes.func,
  children: PropTypes.node,
  id: PropTypes.string,
  className: PropTypes.string
};

export default Button;
