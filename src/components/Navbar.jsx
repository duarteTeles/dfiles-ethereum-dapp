import React from "react";
import {
  Container,
  Navbar,
  NavbarBrand,
  NavbarNav,
  NavbarToggler,
  Collapse,
  NavItem,
  NavLink,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "mdbreact";

import Img from "react-image";
import ImagesUsed from "../components/ImagesUsed";
import cookie from "react-cookies";

// import navbar styles
import "./navbar.scss";

class NavbarItems extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      isWideEnough: false,
      dropdownOpen: false,
      isLoggedIn: false
    };
    this.onClick = this.onClick.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  onClick() {
    this.setState({
      collapse: !this.state.collapse
    });
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  handleLogout() {
    //  cookie.remove('userData', { path: '/' })
    cookie.remove("userData", { path: "/" });
    alert("Logging out...");
    this.setState({ isLoggedIn: false });
  }

  componentDidMount() {
    if (cookie.load("userData") !== undefined) {
      this.setState({ isLoggedIn: true });
    }
  }

  render() {
    return (
      <div>
        <Navbar
          className="navbar-color"
          fixed="top"
          double-nav="true"
          mobile-nofixed="true"
          dark
          expand="md"
          scrolling
          id="navbar"
        >
          <Container>
            <NavLink to="/" style={{ color: "white" }}>
              DFiles
            </NavLink>
            {/* <Img
                src={ImagesUsed.header.src}
                alt="Logo"
                title="Logo"
                height={50}
                width={50}
              />{" "} */}
            {!this.state.isWideEnough && (
              <NavbarToggler onClick={this.onClick} />
            )}
            {/* <Collapse isOpen={this.state.collapse} navbar> */}
            <Collapse navbar>
              <NavbarNav right className="nav-flex-icons font-weight-bold">
                {/* active*/}
                <NavItem>
                  <NavLink to="#">Features</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink to="www.google.com">Get Started</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink to="/files/upload-files">Upload Files</NavLink>
                </NavItem>
                {/* <NavItem>
                  <NavLink to="/files/sharing">Share Files</NavLink>
                </NavItem> */}
                {/* <NavItem>
                  <NavLink to="/auth/account">My Account</NavLink>
                </NavItem> */}

                <NavItem>
                  <NavLink to="/files/my-files">View My Files</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink to="/privacy/privacy-policy">Privacy Policy</NavLink>
                </NavItem>
              </NavbarNav>
              <NavbarNav right>
                <NavItem>
                  {/* <form className="form-inline md-form mt-0">
                              <input className="form-control mr-sm-2 mb-0 text-white" type="text" placeholder="Search" aria-label="Search"/>
                            </form>  */}
                  <NavLink to="#">
                    <i className="fab fa-github" />
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink to="#">
                    <i className="fa fa-book" />
                  </NavLink>
                </NavItem>
              </NavbarNav>
              {!this.state.isLoggedIn ? (
                <React.Fragment>
                  <NavbarNav right>
                    <NavItem>
                      <NavLink to="/auth/login">
                        <button
                          type="button"
                          className="btn btn-primary btn-md"
                        >
                          Login
                        </button>
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink to="/auth/register">
                        <button
                          type="button"
                          className="btn btn-primary btn-md"
                        >
                          Register
                        </button>
                      </NavLink>
                    </NavItem>
                  </NavbarNav>
                </React.Fragment>
              ) : (
                // <NavItem>
                //   {/* <NavLink to="/auth/register"> */}
                //   <button type="button" className="btn btn-primary btn-md">
                //     My Account
                //   </button>
                //   {/* </NavLink> */}
                // </NavItem>
                <NavbarNav right>
                  <NavItem>
                    <Dropdown
                      // isOpen={this.state.dropdownOpen}
                      toggle={this.toggle}
                    >
                      <DropdownToggle nav caret>
                        <i className="fas fa-user-circle fa-3x" />{" "}
                        <span className="ml-2">
                          {" "}
                          {cookie.load("userData").username}
                        </span>
                      </DropdownToggle>
                      <DropdownMenu>
                        <NavLink to="/auth/account">
                          <DropdownItem>Account Settings</DropdownItem>
                        </NavLink>
                        {/* <NavLink to="/auth/account">Account Settings</NavLink> */}
                        {/* <DropdownItem href="/auth/account">
                          Account Settings
                        </DropdownItem> */}
                        <NavLink to="/">
                          <DropdownItem onClick={this.handleLogout}>
                            Logout
                          </DropdownItem>
                        </NavLink>
                        {/* <DropdownItem href="/" onClick={this.handleLogout}>
                          Logout
                        </DropdownItem> */}
                      </DropdownMenu>
                    </Dropdown>
                  </NavItem>
                </NavbarNav>
              )}
            </Collapse>
          </Container>
        </Navbar>
      </div>
    );
  }
}

export default NavbarItems;
