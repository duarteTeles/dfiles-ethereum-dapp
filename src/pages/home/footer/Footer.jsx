import React from "react";
import FooterContent from "./FooterContent";

const Footer = props => {
  return (
    <div>
      {/* Footer */}
      <footer className="page-footer font-small pt-0" id="footer-background">
        <FooterContent />
      </footer>
      {/* Footer */}
    </div>
  );
};

export default Footer;
