import React from "react";

const FooterCopyright = props => {
  return (
    <div className="footer-copyright py-3 text-center">
      <i className="far fa-copyright" /> {(new Date().getFullYear())} DFiles
    </div>
  );
};

export default FooterCopyright;
