import React from "react";

const FooterContentRowTwo = props => {
  return (
    <div className="col-md-2 col-lg-2 col-xl-2  mt-5">
      {/* <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style={{width: 60}}/>  */}
      <p>
        <a href="#!" className="footer-content">
          About
        </a>
      </p>
      <p>
        <a href="#!" className="footer-content">
          Contact
        </a>
      </p>
      <p>
        <a href="#!" className="footer-content">
          F.A.Q.
        </a>
      </p>
    </div>
  );
};

export default FooterContentRowTwo;
