import React from "react";


const FooterContentRowThree = props => {
  return (
    <div className="col-md-4 col-lg-3 col-xl-3 my-5">
      <p>
        <i className="fas fa-file-code mr-3 fa-2x" />
        <a href="#!" className="footer-content">
          Docs
        </a>
      </p>
      <p>
        <i className="fab fa-github-alt mr-3 fa-2x" />
        <a href="#!" className="footer-content">
          Github
        </a>
      </p>
      <p />
    </div>
  );
};

export default FooterContentRowThree;
