import React from "react";

const FooterContentRowOne = () => {
  return(
<div className="col-md-3 col-lg-2 col-xl-2 my-5 ">
  <p>
    <a href="#!" className="footer-content">
      Features
    </a>
  </p>
  <p>
    <a href="#!" className="footer-content">
      Get Started
    </a>
  </p>
</div>
  );
};



export default FooterContentRowOne;
