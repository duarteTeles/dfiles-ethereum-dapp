import React from 'react';
import Img from "react-image";
import ImagesUsed from '../../../../components/ImagesUsed';

const LogoImage = props => {
    return (
       
         <div className="col-md-3 col-lg-4 col-xl-3 my-5">
         {/* <img src="./img/EthDocStore Logo.svg" height="100" className="mx-auto d-block" title="Logo"/> */}
         <Img src={ImagesUsed.footer.LogoImage.src} alt={ImagesUsed.footer.LogoImage.alt} title={ImagesUsed.footer.LogoImage.title} height={ImagesUsed.footer.LogoImage.height} width={ImagesUsed.footer.LogoImage.width} className={ImagesUsed.footer.LogoImage.class}/> 
     </div>
    );
};



export default LogoImage;