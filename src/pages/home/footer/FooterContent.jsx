import React from "react";

import LogoImage from "./footer items/LogoImage";
import FooterContentRowOne from "./footer items/FooterContentRowOne";
import FooterContentRowTwo from "./footer items/FooterContentRowTwo";
import FooterContentRowThree from "./footer items/FooterContentRowThree";
import FooterCopyright from "./FooterCopyright";

const FooterContent = props => {
  return (
    /* Footer Links */
    <div>
    <div className="container text-center text-md-right">
      <div className="row">
        <LogoImage />
        <FooterContentRowOne />
        <FooterContentRowTwo />
        <FooterContentRowThree /> 
       
      </div>
    </div>
     <FooterCopyright/>
     </div>
  );
};



export default FooterContent;
