import React from "react";
import HeadingParagraph from "./SectionOne_DecentralizedTechnologies/HeadingParagraphDecentralizedTechnologies";
import HeadingParagraphItemIPFS from "./SectionOne_DecentralizedTechnologies/Heading Paragraph Items/HeadingParagraphItemOneIPFS";
import HeadingParagraphItemEthereumBlockchain from "./SectionOne_DecentralizedTechnologies/Heading Paragraph Items/HeadingParagraphItemTwoEthereumBlockchain";
import HeadingParagraphItemBigchainDB from "./SectionOne_DecentralizedTechnologies/Heading Paragraph Items/HeadingParagraphItemThreeBigchainDB";

const SectionOne_DecentralizedTechnologies = () => {
  return (
    <div className="container-fluid">
      <div className="container">
        {/* Section: Decentralized Technologies */}
        <section
          id="decentralized-technologies"
          className="text-center text-black"
        >
          <HeadingParagraph />

          {/* Grid Row */}
          <div className="row">
            <HeadingParagraphItemEthereumBlockchain />
            <HeadingParagraphItemIPFS />
            <HeadingParagraphItemBigchainDB />
          </div>
          {/* Grid Row */}
        </section>
        {/* Section DecentralizedTechnologies */}
      </div>
    </div>
  );
};

export default SectionOne_DecentralizedTechnologies;
