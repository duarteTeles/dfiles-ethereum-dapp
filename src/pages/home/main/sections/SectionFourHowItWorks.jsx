import React from "react";
import HeadingParagraphHowItWorks from "./SectionFour_HowItWorks/HeadingParagraph_HowItWorks";

const SectionFourHowItWorks = () => {
  return (
    /* Section: How it Works */
    <section id="section-how-does-it-work" className="text-center text-black">
     <HeadingParagraphHowItWorks/>
    </section>
    /* Section: How it Works */
  );
};

export default SectionFourHowItWorks;
