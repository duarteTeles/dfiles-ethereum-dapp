import React from "react";
import AllFeatures from "./Features/AllFeatures";
import Decentralization from "./Features/Decentralization";
import DocumentImmutability from "./Features/Document Immutability";
import EDSCoin from "./Features/EDSCoin";
import OpenSource from "./Features/OpenSource";
import TrustlessAndSecureDocumentUploadingAndSharing from "./Features/Trustless & Secure Document Uploading and Sharing";


const Features = () => {
  return (
    <div className="container-fluid" id="section-features">
      <div className="container">
        {/* Features Heading  */}
        <h2 className="mb-5 font-weight-bold pt-5">Features</h2>
        {/* Grid row */}
        <div className="row">
          <Decentralization/>
          <DocumentImmutability />
          <OpenSource />
        </div>
        <div className="row">
          <TrustlessAndSecureDocumentUploadingAndSharing />
          <EDSCoin />
        </div>
        <AllFeatures />
      </div>
    </div>
  );
};

export default Features;
