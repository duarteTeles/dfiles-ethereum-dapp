import React from "react";

import Img from "react-image";
import ImagesUsed from "../../../../../../components/ImagesUsed";

const TrustlessAndSecureDocumentUploadingAndSharing = () => {
  return (
    <div className="col-md-6">
      <Img
        src={ImagesUsed.main.sectionThree_Features.TrustlessAndSecureDocumentUploadingAndSharing.src}
        id={ImagesUsed.main.sectionThree_Features.TrustlessAndSecureDocumentUploadingAndSharing.id}
        alt={ImagesUsed.main.sectionThree_Features.TrustlessAndSecureDocumentUploadingAndSharing.alt}
        title={ImagesUsed.main.sectionThree_Features.TrustlessAndSecureDocumentUploadingAndSharing.title}
        height={ImagesUsed.main.sectionThree_Features.TrustlessAndSecureDocumentUploadingAndSharing.height}
        className={ImagesUsed.main.sectionThree_Features.TrustlessAndSecureDocumentUploadingAndSharing.class}
      />
      <h4 className="mt-3">
        Trustless and Secure Document Uploading & Sharing
      </h4>
    </div>
  );
};

export default TrustlessAndSecureDocumentUploadingAndSharing;
