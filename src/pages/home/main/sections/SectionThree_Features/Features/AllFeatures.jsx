import React from 'react';

const AllFeatures = () => {
    return (
        <div className="row">
            <div className="col-md-12 mb-5">
              <button
                type="button"
                className="btn btn-info btn-rounded font-weight-bold buttons-hover btn-lg">
                
                All Features
              </button>
            </div>
          </div>
    );
};

export default AllFeatures;