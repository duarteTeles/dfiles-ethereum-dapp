import React from 'react';
import Img from 'react-image';
import ImagesUsed from "../../../../../../components/ImagesUsed";

const Decentralization = () => {
    return (
        <div className="col-md-4">
        <Img src={ImagesUsed.main.sectionThree_Features.Decentralized.src} id={ImagesUsed.main.sectionThree_Features.Decentralized.id} alt={ImagesUsed.main.sectionThree_Features.Decentralized.alt}
      title={ImagesUsed.main.sectionThree_Features.Decentralized.title}
      height={ImagesUsed.main.sectionThree_Features.Decentralized.height}
      className={ImagesUsed.main.sectionThree_Features.Decentralized.class}/>
        <h4 className="mt-3">
          Decentra-
          <br />lization
        </h4>
      </div>
    );
};

export default Decentralization;