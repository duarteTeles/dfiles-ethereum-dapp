import React from "react";
import Img from "react-image";

import ImagesUsed from "../../../../../../components/ImagesUsed";

const DocumentImmutability = () => {
  return (
    <div className="col-md-4 ">
      <Img
        src={ImagesUsed.main.sectionThree_Features.DocumentImmutability.src}
        id={ImagesUsed.main.sectionThree_Features.DocumentImmutability.id}
        alt={ImagesUsed.main.sectionThree_Features.DocumentImmutability.alt}
        title={ImagesUsed.main.sectionThree_Features.DocumentImmutability.title}
        height={
          ImagesUsed.main.sectionThree_Features.DocumentImmutability.height
        }
        className={
          ImagesUsed.main.sectionThree_Features.DocumentImmutability.class
        }
      />
      <h4 className="mt-3">Document Immutability</h4>
    </div>
  );
};

export default DocumentImmutability;
