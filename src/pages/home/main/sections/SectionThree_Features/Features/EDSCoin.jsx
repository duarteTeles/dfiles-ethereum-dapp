import React from "react";
import Img from 'react-image';
import ImagesUsed from "../../../../../../components/ImagesUsed";


const EDSCoin = () => {
  return (
    <div className="col-md-6">
      <Img src={ImagesUsed.main.sectionThree_Features.EDSCoin.src} id={ImagesUsed.main.sectionThree_Features.EDSCoin.id} alt={ImagesUsed.main.sectionThree_Features.EDSCoin.alt}
      title={ImagesUsed.main.sectionThree_Features.EDSCoin.title}
      height={ImagesUsed.main.sectionThree_Features.EDSCoin.height}
      className={ImagesUsed.main.sectionThree_Features.EDSCoin.class}/>
      <h4 className="mt-3">EDSCoin</h4>
    </div>
  );
};

export default EDSCoin;
