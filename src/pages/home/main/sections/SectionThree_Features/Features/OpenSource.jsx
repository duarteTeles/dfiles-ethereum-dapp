import React from "react";

const OpenSource = () => {
  return (
    <div className="col-md-4">
      <i className="fab fa-osi fa-4x" />
      <h4 className="mt-3">Open-Source</h4>
    </div>
  );
};

export default OpenSource;
