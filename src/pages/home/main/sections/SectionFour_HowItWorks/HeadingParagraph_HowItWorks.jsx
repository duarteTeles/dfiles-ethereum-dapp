import React from 'react';

const HeadingParagraphHowItWorks = () => {
    return (
        <div>
             <div className="container-fluid" id="section-how-it-works">
        <div className="container pb-5">
          {/* How it Works Heading  */}
          <h2 className="mb-5 font-weight-bold pt-5">How It Works</h2>
             <div className="row text-center">
        <div className="col-md-4">
          <h2>1</h2>
          <br/>
          <p className="pt-3 heading-paragraphs">
            An entity (individual user or organization), with an Ethereum
            Wallet, wants to upload a document
          </p> 
        </div>

        <div className="col-md-4">
          <h2>2</h2>
          <br />
          <p className="pt-3 heading-paragraphs">
            The entity pays in EDSCoin to have the document uploaded and
            their metadata stored via BigChain DB
          </p>
        </div>

        <div className="col-md-4">
          <h2>3</h2>
          <br />
          <p className="pt-3 heading-paragraphs">
            Document is encrypted with private and public keys, uploaded to
            an IPFS node and tied to their Ethereum Wallet address
          </p>
        </div>
      </div>

      <div className="row text-center">
        <div className="col-md-4">
          <h2>4</h2>
          <br/>
          <p className="pt-3 heading-paragraphs">
            Documents can be previewed and downloaded, with the entity
            supporting the transaction cost
          </p>
        </div>

        <div className="col-md-4">
          <h2>5</h2>
          <br />
          <p className="pt-3 heading-paragraphs">
            All users have their own dashboard where they can view GDPR
            compliant privacy options{" "}
          </p>
        </div>

        <div className="col-md-4">
          <h2>6</h2>
          <br />
          <p className="pt-3 heading-paragraphs">
            <span className="font-weight-bold">We take only 2%</span> of each
            transaction made{" "}
          </p>
        </div>
      </div>
        </div>
        {/* Grid row */}
      </div>
        </div>
    );
};

export default HeadingParagraphHowItWorks;