import React from "react";

import GDPRRightsWithIcons from "./Heading Paragraph Items/GPDRRightsWithIcons";

const HeadingParagraph = () => {
  return (
    <div>
      {/* Heading  */}
      <h2 className=" font-weight-bold text-center pt-5">GDPR Compliant</h2>

      {/* Grid row */}
      <div className="row d-flex justify-content-center mb-4">
        {/* Grid column */}
        <div className="col-md-8">
          {/* Description  */}
          <p className="pt-3 heading-paragraphs">
            Your privacy is paramount to us: DFiles complies fully with the
            latest European Union's General Data Protection Regulation (GDPR):
          </p>
        </div>
        {/* Grid column */}
      </div>
      {/* Grid row */}
      <GDPRRightsWithIcons />
    </div>
  );
};

HeadingParagraph.propTypes = {};

export default HeadingParagraph;
