import React from 'react';

const PrivacyByDesignPrinciples = () => {
    return (
        <div className="col-md-4">
            <i className="fas fa-user-secret fa-5x"></i>
            <p className="mt-3">Privacy By Design Principles</p>
        </div>
    );
};

export default PrivacyByDesignPrinciples;