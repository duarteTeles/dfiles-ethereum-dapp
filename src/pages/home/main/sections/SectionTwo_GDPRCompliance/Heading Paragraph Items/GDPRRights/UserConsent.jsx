import React from 'react';

const UserConsent = () => {
    return (
        <div className="col-md-8 ">
            <i className="fas fa-user-lock fa-5x"></i>

            <p className="pt-3">Right to user consent</p>
        </div>
    );
};

export default UserConsent;