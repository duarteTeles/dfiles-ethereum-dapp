import React from 'react';

const BeForgotten = () => {
    return (
        <div className="col-md-4">
            <i className="fas fa-user-slash fa-5x"></i>
            <p className="pt-3">
                Right to be forgotten</p>
        </div>
    );
};


export default BeForgotten;