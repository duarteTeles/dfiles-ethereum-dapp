import React from 'react';

const DataPortability = () => {
    return (
        <div className="col-md-8">
            <i className="fas fa-people-carry fa-5x"></i>
            <p className="mt-3">Right to data portability</p>

        </div>
    );
};

export default DataPortability;