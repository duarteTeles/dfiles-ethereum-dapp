import React from 'react';
import Img from 'react-image';

import ImagesUsed from '../../../../../../components/ImagesUsed'
import UserConsent from "./GDPRRights/UserConsent";
import BeForgotten from "./GDPRRights/BeForgotten";
import DataPortability from "./GDPRRights/DataPortability";
import PrivacyByDesignPrinciples from './GDPRRights/PrivacyByDesignPrinciples';

   const GDPRRightsWithIcons = () => 
   {
    return (
          /* Grid row */
          <div className="row">
          {/* Grid column */}
          <div className="col-md-6 text-center">

              <div className="row">
                  <UserConsent/>
                  <BeForgotten/>
              </div>

              <div className="row mt-5">
                 <DataPortability/>
                  <PrivacyByDesignPrinciples/>
              </div>
          </div>
          {/* Grid column */}

          {/* Grid column */}
          <div className="col-md-6 mb-4">
          <Img className={ImagesUsed.main.sectionTwo_GDPRCompliance.gdpr.class} id={ImagesUsed.main.sectionTwo_GDPRCompliance.gdpr.id} src={ImagesUsed.main.sectionTwo_GDPRCompliance.gdpr.src} alt={ImagesUsed.main.sectionTwo_GDPRCompliance.gdpr.alt} title={ImagesUsed.main.sectionTwo_GDPRCompliance.gdpr.title} height={ImagesUsed.main.sectionTwo_GDPRCompliance.gdpr.height} width={ImagesUsed.main.sectionTwo_GDPRCompliance.gdpr.width}/>
          
           

          </div>
          {/* Grid column */}



      </div>
      /* Grid row */
    );
   }
  


    
    


GDPRRightsWithIcons.propTypes = {};

export default GDPRRightsWithIcons;
