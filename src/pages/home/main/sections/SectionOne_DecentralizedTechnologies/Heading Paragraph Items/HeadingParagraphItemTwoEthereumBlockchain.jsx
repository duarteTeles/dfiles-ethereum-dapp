import React from 'react';

const HeadingParagraphItemTwo_EthereumBlockchain = () => 
{
    return( 
             /* Grid Column */
             <div className="col-md-4 mb-5">
             <i className="fab fa-ethereum fa-4x"></i>
             <h4 className="my-4 font-weight-bold">Ethereum Blockchain</h4>
             <p className="heading-paragraph-items"/>Ethereum Blockchain, as a distributed ledger, allows the creation of an immutable record
                 of every file you stored, viewed and shared. It is immune to censorship, DCMA takedowns
                 or unexpected Third parties.
         </div>
          /* Grid Column */
    );
};

export default HeadingParagraphItemTwo_EthereumBlockchain;
