import React from 'react';
import Img from 'react-image';
import ImagesUsed from '../../../../../../components/ImagesUsed';

const HeadingParagraphItemOne_IPFS = () => 
{
    return( 
           /* Grid Column */
           <div className="col-md-4 mb-1">
           <Img id={ImagesUsed.main.sectionOne_DecentralizedTechnologies.ipfs.id} src={ImagesUsed.main.sectionOne_DecentralizedTechnologies.ipfs.src} alt={ImagesUsed.main.sectionOne_DecentralizedTechnologies.ipfs.alt} title={ImagesUsed.main.sectionOne_DecentralizedTechnologies.ipfs.title} height={ImagesUsed.main.sectionOne_DecentralizedTechnologies.ipfs.height} width={ImagesUsed.main.sectionOne_DecentralizedTechnologies.ipfs.width}/>
               <h4 className="my-4 font-weight-bold">IPFS</h4>
               <p className="heading-paragraph-items">The
                   <a href="https://ipfs.io/">Interplanetary File System </a>is the decentralized technology responsible for file storage
                   and sharing between parties.</p>
           </div>
           /* Grid Column */
    );
};

export default HeadingParagraphItemOne_IPFS;
