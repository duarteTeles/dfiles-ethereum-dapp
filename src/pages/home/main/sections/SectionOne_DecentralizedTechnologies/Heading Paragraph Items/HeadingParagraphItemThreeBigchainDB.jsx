import React from 'react';
import Img from 'react-image';
import ImagesUsed from '../../../../../../components/ImagesUsed';

const HeadingParagraphItemThree_BigchainDB = () => 
{
    return( 
           /* Grid Column */
           <div className="col-md-4 mb-1">
           <Img id={ImagesUsed.main.sectionOne_DecentralizedTechnologies.bigchainDB.id} src={ImagesUsed.main.sectionOne_DecentralizedTechnologies.bigchainDB.src} alt={ImagesUsed.main.sectionOne_DecentralizedTechnologies.bigchainDB.alt} title={ImagesUsed.main.sectionOne_DecentralizedTechnologies.bigchainDB.title} height={ImagesUsed.main.sectionOne_DecentralizedTechnologies.bigchainDB.height} width={ImagesUsed.main.sectionOne_DecentralizedTechnologies.bigchainDB.width}/>
              
               <h4 className="my-4 font-weight-bold">BigChain Database</h4>
               <p className="heading-paragraph-items">The Bigchain Database is responsible for file metadata storage. Its decentralized approach
                   ensures it cannot be taken down or tampered with. </p>
           </div>
           /* Grid Column */
    );
};

export default HeadingParagraphItemThree_BigchainDB;
