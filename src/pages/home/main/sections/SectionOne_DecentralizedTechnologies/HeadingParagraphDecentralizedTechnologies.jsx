import React from 'react';

const HeadingParagraph = () => {

    return (
        <div>
            {/* Heading */}
            <h2 className="mb-5 font-weight-bold">Built on Decentralized Technologies</h2>

            {/* Grid Row */}
            <div className="row d-flex justify-content-center mb-4">

                 {/* Grid Row */}
            <div className="col-md-8">
                <p className="heading-paragraphs">
                    Traditional file storage and sharing rely on complex, centralized
                    infrastructures which are prone to downtime and can be painfully slow. Your
                    precious documents might be deleted at any time; as a target of DCMA takedowns
                    or hackers. They can also be modified or obtained without your consent by
                    unexpected Third parties. {/* These infrastructures are controlled by big companies where transparency is not their strong suit; the data in your critical documents might be sold or viewed without your explicit consent.
                                    <br> */}
                    <br/>
                    We envision decentralization as an easy, secure method of file storage and
                    sharing. DFiles is built on <span className="font-weight-bold"> 3 core decentralized technologies:</span>
                </p>
            </div>

            </div>

        </div>
    );
};

HeadingParagraph.propTypes = {};

export default HeadingParagraph;
