import React from 'react';
import HeadingParagraph from "./SectionTwo_GDPRCompliance/HeadingParagraph";


class SectionTwo_GDPRCompliance extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (

    //   Section: GDPR compliance
            <section id="gdpr-compliance">
                <div className="container-fluid" id="section-gdpr">
                    <div className="container text-white">
                    <HeadingParagraph/>

                    </div>
                </div>
        </section>
   
        );
    }
}

SectionTwo_GDPRCompliance.propTypes = {};

export default SectionTwo_GDPRCompliance;
