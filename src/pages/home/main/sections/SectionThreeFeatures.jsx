import React from "react";
import HeadingParagraphFeatures from './SectionThree_Features/HeadingParagraphFeatures';

const SectionThree_Features = () => {
  return (
    //   Need to add these features:
    // Embraced Audited Code
    // Read Our Comprehensive Documentation
    // No Hidden Fees
    // Document and Metadata Encryption
    // Preview Uploaded Files
    // Trustless File Sharing

    <div>
      {/* Section: Best Features */}
      <section id="best-features" className="text-center text-black">
       <HeadingParagraphFeatures/>
      </section>
    </div>
    /* Section: Best Features */
  );
};

export default SectionThree_Features;
