import React from "react";
import SectionOneDecentralizedTechnologies from "./sections/SectionOneDecentralizedTechnologies";
import SectionTwoGPDRCompliance from "./sections/SectionTwoGPDRCompliance";
import SectionThreeFeatures from "./sections/SectionThreeFeatures";
import SectionFourHowItWorks from "./sections/SectionFourHowItWorks";

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <main className="mt-5">
          <SectionOneDecentralizedTechnologies />
          <SectionTwoGPDRCompliance />
          <SectionThreeFeatures />
          <SectionFourHowItWorks />
        </main>
      </div>
    );
  }
}

Main.propTypes = {};

export default Main;
