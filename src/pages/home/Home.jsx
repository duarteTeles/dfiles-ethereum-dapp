import React from 'react';
import Navbar from '../../components/Navbar';
import Header from "./Header/Header";
import Main from "./main/Main";
import Footer from './footer/Footer';



class Home extends React.Component {
    render() {
        return (
            <header>
                <div>
                    {/* Navbar  */}
                    <Navbar/>
                    {/* Header = introduction  */}
                    <Header/>
                    {/* main content, including all the sections */}
                    <Main/>
                    {/* Footer */}
                    <Footer/>
                </div>
            </header>
        );
    }
}

export default Home;