import React from "react";
import Button from "../../../../components/Button.jsx";

class IntroText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="col-md-10 col-12 col-sm-12">
        <h2
          className="col-12 font-weight-bold white-text pt-5 mb-2"
          id="cover-main-header"
        >
          Easily Share and Upload Your Important Documents
        </h2>

        <hr className="hr-light" />

        <h4 className="white-text my-4">
          DFiles is a decentralized, open-source and easy-to-use platform that
          enables trustless document storage and visualization between parties.
        </h4>

        <div className="container mt-5">
          <div>
            <Button
              rounded
              color="indigo"
              size="lg"
              href="/"
              className="mr-4 buttons-hover"
            >
              Get Started
              <i
                className="fas fa-chevron-circle-right ml-1 "
                style={{
                  fontSize: 1.5 + "em"
                }}
              />
            </Button>

            <Button
              color="elegant"
              rounded
              size="lg"
              className="buttons-hover my-3"
            >
              Documentation
              <i
                className=" fa fa-book ml-1"
                style={{
                  fontSize: "1.5em"
                }}
              />
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default IntroText;
