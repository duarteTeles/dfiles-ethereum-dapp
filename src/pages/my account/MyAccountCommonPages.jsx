import React from "react";
import PropTypes from "prop-types";

import NavbarItems from "../../components/Navbar";
import cookie from "react-cookies";
import MyAccountSidebar from "./MyAccountSidebar";
import { Link } from "react-router-dom";

class MyAccountCommonPages extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoggedIn: false };
  }

  async componentDidMount() {
    if (cookie.load("userData") !== undefined) {
      this.setState({ isLoggedIn: true });
    }
  }

  render() {
    if (!this.state.isLoggedIn) {
      return (
        <div className="container mr-5 pt-5">
          <NavbarItems />{" "}
          <h4>
            Please{" "}
            <Link to={`/auth/login`}>
              {" "}
              <strong>Login </strong>
            </Link>
            to view this page.
          </h4>
        </div>
      );
    } else {
      return (
        <div className="container mr-5">
          <NavbarItems />
          <div>
            <div className="row pt-4">
              <MyAccountSidebar />
            </div>
          </div>
        </div>
      );
    }
  }
}

MyAccountCommonPages.propTypes = {};

export default MyAccountCommonPages;
