import fakeAuth from "../../utils/fakeAuth";
import { withRouter } from "react-router-dom";

import React from "react";

import NavbarItems from "../../components/Navbar";
import axios from "axios";
import cookie from "react-cookies";
import "./_myAccount.scss";
import MyAccountSidebar from "./MyAccountSidebar";
import { Link } from "react-router-dom";
import { stringify } from "querystring";
import DownloadUserData from "./DownloadUserData";
import DownloadUserDataPortability from "./DownloadUserDataPortability";
import DeleteUserAccount from "./DeleteUserAccount";

class MyAccount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: [],
      userLoggedIn: false
    };
  }

  async componentDidMount() {
    //   const username = cookie.load("userData").username;
    //        console.log("userData2 ",cookie.load("userData"));
    if (cookie.load("userData") !== undefined) {
      this.setState({ userData: cookie.load("userData") });
      this.setState({ userLoggedIn: true });
    }
  }

  //   //TODO: Change route to login after having a form to validate password

  //   axios
  //     .get(`http://localhost:3005/users/${username}`)
  //     .then(response => {
  //       console.log(response);
  //       let userDataTemp = {
  //         username: response.data.username,
  //         email: response.data.email,
  //         ethereumAccountAddress: response.data.ethereumAccountAddress,
  //         privateKey: response.data.privateKey,
  //         firstName: response.data.firstName,
  //         lastName: response.data.lastName
  //       };
  //       this.setState({ userData: userDataTemp });
  //     })
  //     .catch(function(error) {
  //       console.log("error: ", error);
  //       if (!error.response) {
  //         alert("Network error, try again");
  //       } else {
  //         if (error.response) {
  //           if (error.response.status === 404) {
  //             alert("Invalid username or password");
  //           } else if (error.response.status === 500) {
  //             alert("A serious error has occurred");
  //           }
  //         }
  //       }
  //     });
  // }

  render() {
    console.log("ggggg ggggg ", this.props);
    return (
      <div className="container mr-5">
        <NavbarItems />
        {this.state.userLoggedIn ? (
          <div>
            <div className="row pt-4">
              <MyAccountSidebar />
              <div
                id="content"
                className="col-md-6 ml-2 d-flex justify-content-between"
              >
                <h4 className="text-left contentTextTitle">Account Details</h4>
                <h4 className="text-right contentText">
                  <Link to={`/auth/account/settings`}>Edit</Link>
                </h4>
              </div>
            </div>
            <div className="row mt-2">
              <div className="offset-md-4 col-md-6 ">
                <table>
                  <tbody>
                    <tr>
                      <th className="align-top">Username:</th>
                      <td className="accountTableData">
                        {this.state.userData.username}
                      </td>
                    </tr>
                    {/* <tr>
                    <th className="align-top">Name:</th>
                    <td className="accountTableData">
                      {`${this.state.userData.firstName} ${
                        this.state.userData.lastName
                      }`}
                    </td>
                  </tr> */}
                    <tr>
                      <th className="align-top">Ethereum Address:</th>
                      <td className="accountTableData">
                        {" "}
                        {this.state.userData.ethereumAccountAddress}
                      </td>
                    </tr>
                    <tr>
                      <th className="align-top">Email:</th>
                      <td className="accountTableData">
                        {" "}
                        {this.state.userData.email}
                      </td>
                    </tr>
                    <tr>
                      <th className="align-top">Password:</th>
                      <td className="accountTableData">
                        **************{" "}
                        <Link to={`/auth/account/settings`}>(Change)</Link>
                      </td>
                    </tr>
                    <tr>
                      <th className="align-top font-weight-bold">
                        Access your complete data:
                      </th>
                      <td className="accountTableData">
                        <DownloadUserData />
                      </td>
                    </tr>
                    <tr>
                      <th className="align-top font-weight-bold">
                        Data portability: Download your data
                      </th>
                      <td className="accountTableData">
                        <DownloadUserDataPortability />
                      </td>
                    </tr>
                    <tr>
                      <th className="align-top font-weight-bold">
                        Delete Your Account
                      </th>
                      <td className="accountTableData">
                        {console.log("... ", this.props)}
                        <DeleteUserAccount
                          BrowserHistory={this.props.history}
                        />
                      </td>
                    </tr>
                    {/* <tr>
                    <th className="align-top">Private Key:</th>
                    <td className="accountTableData">
                      {" "}
                      {this.state.userData.privateKey}
                    </td>
                  </tr> */}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        ) : (
          <div className="pt-5">
            <h4>
              Please{" "}
              <Link to={`/auth/login`}>
                <strong>Login</strong>{" "}
              </Link>{" "}
              to view this page.
            </h4>
          </div>
        )}
      </div>
    );
  }
}

MyAccount.propTypes = {};

export default MyAccount;
