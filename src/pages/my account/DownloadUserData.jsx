import React, { Component } from "react";
import cookie from "react-cookies";
import fileDownloader from "js-file-download";

import FilesContract from "../../../build/contracts/Files.json";
const contract = require("truffle-contract");
const filesContract = contract(FilesContract);
import getWeb3 from "../../utils/getWeb3";
import { formatBytes } from "../../utils/UtilFunctions";
const ipfsFileUrl = `https://gateway.ipfs.io/ipfs/`;
import ReactLoading from "react-loading";
import axios from "axios";

const _ = require("underscore");

export default class DownloadUserData extends Component {
  constructor(props) {
    super(props);

    this.readAllStoredFilesInBlockchain = this.readAllStoredFilesInBlockchain.bind(
      this
    );

    this.convertUserDataObjectToJsonAndDownload = this.convertUserDataObjectToJsonAndDownload.bind(
      this
    );
    this.state = {
      accountFrom: null,
      userPrivateKey: null,
      isUserLoggedIn: false,
      web3: null,
      userData: [],
      isFetchingFiles: true
    };
  }

  convertUserDataObjectToJsonAndDownload() {
    console.log("new files state array2 ", this.state.userData);
    console.log("ggggg");
    // let userData = this.state.userData;
    console.log("userData ", this.state.userData);

    let filename = "userData.json";
    let contentType = "application/json;charset=utf-8;";
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      var blob = new Blob(
        [decodeURIComponent(encodeURI(JSON.stringify(this.state.userData)))],
        { type: contentType }
      );
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      var a = document.createElement("a");
      a.download = filename;
      a.href =
        "data:" +
        contentType +
        "," +
        encodeURIComponent(JSON.stringify(this.state.userData));
      a.target = "_blank";
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
  }

  async readAllStoredFilesInBlockchain() {
    // let filesContractInstance = await filesContract.deployed();
    let filesContractInstance = await filesContract.at(
      "0x37b143616dA99BB2A9bC6F2b01eD4F0872343654"
    );

    // If using web3 straight away, we need the abi

    // let filesContractInstance = await filesContract.at(
    //   "0x64F18cC73412d5809958256dAd881E4A9B06134A"
    // );

    let fileObject = [];

    // let userData = {
    //   username: "gg",
    //   email: "gg",
    //   ethereumAccountAddress: "gg",
    //   privateKey: "gg",
    //   publicKey: "gg",
    //   files: [
    //     {
    //       name: "",
    //       hash: "",
    //       extension: "",
    //       size: 0,
    //       timestamp: ""
    //     }
    //   ]
    // };

    if (this.state.accountFrom !== undefined) {
      try {
        const filesBlockchainArrayLength = (await filesContractInstance.getUserFilesLength()).toNumber();
        if (filesBlockchainArrayLength === 0) {
          console.log("false");
        }

        // let files = this.state.userData.slice(); //copy the array
        let files = [];
        // let mimeType = "";
        for (let index = 0; index < filesBlockchainArrayLength; index++) {
          const file = await filesContractInstance.getFileAtIndex(index);
          console.log("fileeee ", file[5]);
          if (
            file &&
            this.state.accountFrom.toLowerCase() === file[5].toLowerCase()
          ) {
            console.log("file ", file);
            console.log("index ", index);
            fileObject = {
              // name: (

              //     {/* <a
              //       // data-tip
              //       // data-for={`global${index}`}
              //       href={`${ipfsFileUrl}${file[3]}`}
              //       target="_blank"
              //     > */}
              //     {/* </a> */}
              //     file[0]

              // ),

              // @ts-ignore
              name: file[0],
              extension: file[1],
              url: `${ipfsFileUrl}${file[3]}`,
              // metadata: file[4],
              size: formatBytes(file[2].toNumber(), 2),
              hash: file[3],
              timestamp: file[4],
              ethereumAccountAddress: file[5]
            };

            files.push(fileObject);
          }
        }

        // if (
        //   files[0].ethereumAccount.toLowerCase() ===
        //   this.state.accountFrom.toLowerCase()
        // ) {
        // //localhost:3005/users/userByEthereumAddress?ethereumAccountAddress=0xaa6C153202Ac204dAE3C62a3E5Bc0b851F10886D
        let userDataTemp, username;
        console.log("gg ", files[0]);

        // `http://localhost:3005/users/userByEthereumAddress?ethereumAccountAddress=0xaa6c153202ac204dae3c62a3e5bc0b851f10886d`
        axios
          .get(
          `http://localhost:3005/users/userByEthereumAddress?ethereumAccountAddress=${this.state.accountFrom}`
          )
          .then(response => {

            username = response.data.username;
            console.log("response.data.username ", response.data.username);
            userDataTemp = {
              username: response.data.username,
              email: response.data.email,
              ethereumAccountAddress: response.data.ethereumAccountAddress,
              privateKey: response.data.privateKey,
              publicKey: response.data.publicKey,
              files: { ...files }
              // firstName: response.data.firstName,
              // lastName: response.data.lastName
            };
            this.setState({ isFetchingFiles: false });

            this.setState({ userData: userDataTemp });
            console.log("gg ", this.state.userData);
          });

        console.log("new files state array ", this.state.userData);
        this.setState({ isFetchingFiles: false });
        // this.convertUserDataObjectToJsonAndDownload();
        // if (cookie.load("userData").username === rows.username} )
        // this.setState({ rows: files });
      } catch (error) {
        console.log("error: ", error);
      }
    }
  }

  async componentDidMount() {
    // get User private key from cookie
    if (cookie.load("userData") !== undefined) {
      this.setState({ userPrivateKey: cookie.load("userData").privateKey });
    }
    // Get network provider and web3 filesContract. See utils/getWeb3 for more info.
    try {
      let results = await getWeb3;
      this.setState({ web3: results.web3 });
      filesContract.setProvider(this.state.web3.currentProvider);
      // Instantiate contract once web3 provided.
    } catch (error) {
      console.log("Metamask not found.", error);
    }

    this.state.web3.eth.getAccounts(async (error, accounts) => {
      if (error) {
        console.log("wtf error", error);
      } else {
        console.log("accounts: ", accounts);
        if (accounts) {
          this.setState({
            accountFrom: accounts[0].toLowerCase()
          });
        }
      }
    });

    this.readAllStoredFilesInBlockchain();
  }

  render() {
    if (this.state.isFetchingFiles) {
      // return <p>Loading, please wait!!!!</p>;
      return (
        <div className="d-flex align-items-center, justify-content-center">
          <ReactLoading
            type={"spin"}
            color={"#0000ff"}
            height={100}
            width={100}
          />
          <h4 className="pt-3 pl-3 ml-3">Loading Files!</h4>
        </div>
      );
    }
    // if (cookie.load("userData") === undefined) {
    //   return (
    //     <div>
    //       <FilesCommonPages>
    //         <h4>
    //           Please{" "}
    //           <Link to={`/auth/login`}>
    //             {" "}
    //             <strong>Login </strong>
    //           </Link>
    //           to view this page.
    //         </h4>
    //       </FilesCommonPages>
    //     </div>
    //   );
    return (
      <button
        className="btn btn-primary"
        onClick={this.convertUserDataObjectToJsonAndDownload}
      >
        Download my data
      </button>
    );
  }
}
