import React from "react";
import PropTypes from "prop-types";
import NavbarItems from "../../components/Navbar";
import MyAccountSidebar from "./MyAccountSidebar";
import "./_myAccountSettings.scss";
import { withFormik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import cookie from "react-cookies";
import getWeb3 from "../../utils/getWeb3";
import Select from "react-select";

const ethereumWalletAccountsList = [
  // { value: "Food", label: "Food" },
  // { value: "Being Fabulous", label: "Being Fabulous" },
  // { value: "Ken Wheeler", label: "Ken Wheeler" },
  // { value: "ReasonML", label: "ReasonML" },
  // { value: "Unicorns", label: "Unicorns" },
  // { value: "Kittens", label: "Kittens" }
];

class MySelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      web3: null,
      ethereumAccounts: []
    };
  }

  async componentDidMount() {
    try {
      let results = await getWeb3;
      this.setState({ web3: results.web3 });
    } catch (error) {
      console.log("Metamask not found.", error);
      this.setState({ web3: null });
    }

    if (this.state.web3 !== undefined) {
      this.state.web3.eth.getAccounts(async (error, accounts) => {
        if (error) {
          console.log("wtf error 2", error);
        } else {
          for (let i = 0; i < accounts.length; i++) {
            ethereumWalletAccountsList.push({
              value: accounts[i],
              label: accounts[i].toString()
            });
          }

          console.log("accounts: ", accounts);
          this.setState({ ethereumAccounts: accounts });
          console.log("Ethereum Accounts State ", this.state.ethereumAccounts);
        }
      });
    }
  }

  handleChange = value => {
    // this is going to call setFieldValue and manually update values.
    this.props.onChange("ethereumAccountAddress", value);
  };

  handleBlur = () => {
    // this is going to call setFieldTouched and manually update touched.
    this.props.onBlur("ethereumAccountAddress", true);
  };

  placeholderTextChangeEthereumAddress = (
    ethereumAccountsSize,
    ethereumAccountAddress
  ) => {
    if (this.state.web3) {
      if (ethereumAccountsSize === 0) {
        return ethereumAccountAddress.toString();
      } else if (ethereumAccountsSize > 0) {
        return "Select Ethereum Account Address";
      }
    } else {
      return "Metamask not found. Please install the Metamask extension and create an Ethereum Wallet to continue";
    }
  };


  render() {
    const ethereumAccountAddress = cookie.load("userData")
      .ethereumAccountAddress;
    return (
      <div style={{ margin: "1rem 0" }}>
        <label htmlFor="color">
          <strong>Ethereum Account Address </strong>{" "}
        </label>
        <Select
          id="color"
          options={
            ethereumWalletAccountsList.length > 0
              ? ethereumWalletAccountsList
              : []
          }
          autoFocus={true}
          multi={false}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={
            ethereumWalletAccountsList.length > 0
              ? this.props.value
              : ethereumAccountAddress
          }
          isDisabled={this.state.web3 ? false : true}
          placeholder=
        {

            this.placeholderTextChangeEthereumAddress(
            ethereumWalletAccountsList.length,
            ethereumAccountAddress
          )}
          noOptionsMessage={() => "No Ethereum accounts found."}
        />
        {!!this.props.error &&
          this.props.touched && (
            <div style={{ color: "red", marginTop: ".5rem" }}>
              {this.props.error}
            </div>
          )}
      </div>
    );
  }
}

class MyAccountSettingsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const email = cookie.load("userData").email;

    return (
      <form onSubmit={this.props.handleSubmit}>
        <h3>Edit Personal Information</h3>
        <div className="form-group">
          <label
            htmlFor="email"
            style={{ display: "block", marginTop: ".5rem" }}
          >
            <strong>Email</strong>
          </label>
          <input
            id="email"
            autoComplete="email"
            placeholder={email}
            type="email"
            value={this.props.values.email}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.email &&
            this.props.touched.email && (
              <div style={{ color: "red", marginTop: ".5rem" }}>
                {this.props.errors.email}
              </div>
            )}
        </div>
        <div className="form-group">
          <MySelect
            value={this.props.values.ethereumAccountAddress}
            onChange={this.props.setFieldValue}
            onBlur={this.props.setFieldTouched}
            error={this.props.errors.ethereumAccountAddress}
            touched={this.props.touched.ethereumAccountAddress}
            // placeholder={this.placeholderTextChangeEthereumAddress(
            //   ethereumWalletAccountsList.length,
            //   ethereumAccountAddress
            // )}
          />
        </div>

        <div className="form-group">
          <h3>Change Password</h3>
          <label
            htmlFor="currentPassword"
            style={{ display: "block", marginTop: ".5rem" }}
          >
            <strong>Current Password</strong>
          </label>
          <input
            id="currentPassword"
            autoComplete="off"
            placeholder="Enter your current password"
            type="password"
            value={this.props.values.password}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.currentPassword &&
            this.props.touched.currentPassword && (
              <div style={{ color: "red", marginTop: ".5rem" }}>
                {this.props.errors.currentPassword}
              </div>
            )}
        </div>

        <div className="form-group">
          <label
            htmlFor="newPassword"
            style={{ display: "block", marginTop: ".5rem" }}
          >
            <strong>New Password</strong>
          </label>
          <input
            id="newPassword"
            placeholder="Enter new password"
            autoComplete="off"
            type="password"
            value={this.props.values.newPassword}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.newPassword &&
            this.props.touched.newPassword && (
              <div style={{ color: "red", marginTop: ".5rem" }}>
                {this.props.errors.newPassword}
              </div>
            )}
        </div>
        <div className="form-group">
          <label
            htmlFor="confirmNewPassword"
            style={{ display: "block", marginTop: ".5rem" }}
          >
            <strong>Confirm New Password</strong>
          </label>
          <input
            id="confirmNewPassword"
            autoComplete="off"
            placeholder="Enter your current password"
            type="password"
            value={this.props.values.confirmNewPassword}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.confirmNewPassword &&
            this.props.touched.confirmNewPassword && (
              <div style={{ color: "red", marginTop: ".5rem" }}>
                {this.props.errors.confirmNewPassword}
              </div>
            )}
        </div>

        <button
          type="button"
          className="outline btn btn-default"
          onClick={this.props.handleReset}
          disabled={!this.props.dirty || this.props.isSubmitting}
        >
          Reset
        </button>
        <button
          type="submit"
          className={"btn btn-primary"}
           onClick={this.props.handleSubmit}
          disabled={this.props.isSubmitting}
        >
          Submit
        </button>
      </form>
    );
  }
}

const MyAccountSettingsEnhancedForm = withFormik({
  mapPropsToValues: props => ({
    email: "",
    currentPassword: "",
    newPassword: "",
    confirmNewPassword: "",
    ethereumAccountAddress: []
  }),
  validationSchema: Yup.object().shape({
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required"),
    currentPassword: Yup.string().required("Current password is required"),
    newPassword: Yup.string().required("New password is required"),
    confirmNewPassword: Yup.string()
      .oneOf([Yup.ref("newPassword")], "Passwords do not match")
      .required("Password confirm is required")
  }),

  handleSubmit: (values, { setSubmitting, props }) => {
    event.preventDefault();
    // setTimeout(() => {
    //   alert(JSON.stringify(values, null, 2));
    //   setSubmitting(false);
    // }, 1000);

    //TODO: validate input form with valid user password
    const username = cookie.load("userData").username;
    console.log("valuueeeeees ", values);
    axios
      .get(
        // `http://localhost:3005/users/login?username=${username}&password=${
          `http://localhost:3005/users/login?username=${username}&password=${values.currentPassword
        }`
      )
      .then(function(response) {
        let updateUserInfo= {}
        if (values.email.length>0)
        {
          updateUserInfo.email = values.email;
        }
        else
        {
          updateUserInfo.email = cookie.load("userData").email;
        }
        if (values.ethereumAccountAddress.value.length>0)
        {
          updateUserInfo.ethereumAccountAddress = values.ethereumAccountAddress.value;
        }
        else
        {
          updateUserInfo.ethereumAccountAddress = cookie.load("userData").ethereumAccountAddress;
        }
        updateUserInfo.password = values.newPassword;


        // http://localhost:3005/users/gg
        axios.put( `http://localhost:3005/users/${username}`, updateUserInfo)

      .then(function(response) {
        //http://localhost:3005/users/gg
        alert("update successful");

        props.history.push("/auth/account");



      })
      // .catch(function(error) {
      //   console.log("error: ", error);
      //   if (
      //     !error.response ||
      //     error.response.status === 404 ||
      //     error.response.status === 500
      //   ) {
      //     alert("update unsuccessful");
      //   }
      // });

      })
      .catch(function(error) {
        console.log("error: ", error);
        if (
          !error.response ||
          error.response.status === 404 ||
          error.response.status === 500
        ) {
          alert("invalid output");
        }
      });

    setSubmitting(false);
  },

  displayName: "MyAccountSettingsForm" // helps with React DevTools
})(MyAccountSettingsForm);

class MyAccountSettings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false
    };
  }

  async componentDidMount() {
    if (cookie.load("userData") !== undefined) {
      this.setState({ isLoggedIn: true });
    }
  }

  render() {

    if (this.state.isLoggedIn) {
      return (
        <div className="container mr-5">
          <NavbarItems />
          <div>
            <div className="row pt-4">
              <MyAccountSidebar />
              <div id="content " className="col-md-6 ml-2  mt-5">

                <MyAccountSettingsEnhancedForm />
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="container mr-5">
          <NavbarItems />
          <div>
            <div className="row pt-4">
              <MyAccountSidebar />
            </div>
          </div>
        </div>
      );
    }
  }
}

MyAccountSettings.propTypes = {};

export default MyAccountSettings;
