import React from "react";
import "./_myAccount.scss";
import cookie from "react-cookies";
import NavbarItems from "../../components/Navbar";
import { Link } from "react-router-dom";

class MyAccountSidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoggedIn: false };
  }

  async componentDidMount() {
    if (cookie.load("userData") !== undefined) {
      this.setState({ isLoggedIn: true });
    }
  }

  render() {
    if (!this.state.isLoggedIn) {
      return (
        <div className="container mr-5 pt-5">
          <NavbarItems />{" "}
          <h4>
            Please{" "}
            <Link to={`/auth/login`}>
              {" "}
              <strong>Login </strong>
            </Link>
            to view this page.
          </h4>
        </div>
      );
    } else {
      return (
        <div id="sidebar" className="col-md-4">
          <h1 className="contentTitle ">Settings</h1>
          <ul className="mt-5">
            <li>
              <i className="fas fa-user" style={{ color: "blue" }} />
              <span className="sidebarText">
                {" "}
                <Link to={`/auth/account`}>Overview</Link>
              </span>
            </li>
            <li>
              <i className="fas fa-lock" style={{ color: "grey" }} />
              <span className="sidebarText">
                <Link to={`/auth/account/settings`}>Account Settings</Link>
              </span>
            </li>
            {/* <li>
              {" "}
              <i className="fas fa-user-secret" />
              {/* <span className="sidebarText">
                <Link to={`/auth/account/privacy`}>
                  Privacy and GDPR Settings
                </Link>
              </span> */}
            {/* </li> */}
          </ul>
        </div>
      );
    }
  }
}

export default MyAccountSidebar;
