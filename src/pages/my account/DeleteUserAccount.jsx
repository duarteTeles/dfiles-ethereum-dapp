import React, { Component } from "react";
import cookie from "react-cookies";
import  browserHistory  from 'react-router-dom'

import axios from "axios";

const _ = require("underscore");

 class DeleteUserAccount extends Component {
  constructor(props) {
    super(props);


    this.state = {

      isUserLoggedIn: false,
      history: this.props.BrowserHistory,
      };
      this.deleteUserAccount.bind(this)
  }

  deleteUserAccount(history)
  {


      let username=null;

 if (cookie.load("userData") !== undefined) {
      username = cookie.load("userData").username;
    }


      axios
      .delete(
        `http://localhost:3005/users/${
         username
        }`
      )
      .then(response => {


        if (cookie.load("userData") !== undefined) {
          cookie.remove("userData", { path: "/" });
        }
        alert ("user deleted successfully!");

 history.push("/")

      })
      .catch(function(error) {
          if (error.response) {
            if (error.response.status === 404) {
              alert("User not found");
            } else if (error.response.status === 500) {
              alert("Failed to delete user");
            }

          }
      });



  }


  async componentDidMount() {
  }

  render() {

    return (
      <button
        className="btn btn-primary"
        // onClick={this.deleteUserAccount}
        onClick={() => this.deleteUserAccount(this.props.BrowserHistory)}
      >
        Delete My Account
      </button>
    );
  }
}


export default DeleteUserAccount;