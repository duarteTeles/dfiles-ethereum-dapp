import React, { Component } from "react";
import cookie from "react-cookies";
import fileDownloader from "js-file-download";

import FilesContract from "../../../build/contracts/Files.json";
const contract = require("truffle-contract");
const filesContract = contract(FilesContract);
import getWeb3 from "../../utils/getWeb3";
import { formatBytes } from "../../utils/UtilFunctions";
const ipfsFileUrl = `https://gateway.ipfs.io/ipfs/`;
import ReactLoading from "react-loading";
import axios from "axios";

const _ = require("underscore");

export default class DownloadUserDataPortability extends Component {
  constructor(props) {
    super(props);

    this.readUserDataFromDatabase = this.readUserDataFromDatabase.bind(this);

    this.downloadUserDataPortability = this.downloadUserDataPortability.bind(
      this
    );
    this.state = {
      accountFrom: null,
      userPrivateKey: null,
      isUserLoggedIn: false,
      web3: null,
      userData: [],
      isFetchingFiles: true
    };
  }

  downloadUserDataPortability() {
    let filename = "userDataPortability.json";
    let contentType = "application/json;charset=utf-8;";
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      var blob = new Blob(
        [decodeURIComponent(encodeURI(JSON.stringify(this.state.userData)))],
        { type: contentType }
      );
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      var a = document.createElement("a");
      a.download = filename;
      a.href =
        "data:" +
        contentType +
        "," +
        encodeURIComponent(JSON.stringify(this.state.userData));
      a.target = "_blank";
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
  }

  async componentDidMount() {
    // get User private key from cookie
    if (cookie.load("userData") !== undefined) {
      this.setState({ userPrivateKey: cookie.load("userData").privateKey });
    }
    // Get network provider and web3 filesContract. See utils/getWeb3 for more info.
    try {
      let results = await getWeb3;
      this.setState({ web3: results.web3 });
      filesContract.setProvider(this.state.web3.currentProvider);
      // Instantiate contract once web3 provided.
    } catch (error) {
      console.log("Metamask not found.", error);
    }

    this.state.web3.eth.getAccounts(async (error, accounts) => {
      if (error) {
        console.log("wtf error", error);
      } else {
        console.log("accounts2: ", accounts[0].toLowerCase());
        if (accounts) {
          this.setState({
            accountFrom: accounts[0].toLowerCase()
          });
        }
      }
    });

    this.readUserDataFromDatabase();
  }

  async readUserDataFromDatabase() {
    let filesContractInstance = await filesContract.at(
      "0x37b143616dA99BB2A9bC6F2b01eD4F0872343654"
    );


    // if (
    //   files[0].ethereumAccount.toLowerCase() ===
    //   this.state.accountFrom.toLowerCase()
    // ) {
    // //localhost:3005/users/userByEthereumAddress?ethereumAccountAddress=0xaa6C153202Ac204dAE3C62a3E5Bc0b851F10886D
    let userDataTemp, username;

    axios
      .get(
        `http://localhost:3005/users/userByEthereumAddress?ethereumAccountAddress=${
          this.state.accountFrom
        }`
      )
      .then(response => {
        username = response.data.username;
        console.log("response.data.username ", response.data.username);
        userDataTemp = {
          username: response.data.username,
          email: response.data.email,
          ethereumAccountAddress: response.data.ethereumAccountAddress,
          privateKey: response.data.privateKey,
          publicKey: response.data.publicKey
          // firstName: response.data.firstName,
          // lastName: response.data.lastName
        };
        this.setState({ isFetchingFiles: false });

        this.setState({ userData: userDataTemp });
        console.log("gg ", this.state.userData);
        console.log("gg2 ", userDataTemp);
      });

    console.log("new files state array ", this.state.userData);
    this.setState({ isFetchingFiles: false });
  }
  render() {
    if (this.state.isFetchingFiles) {
      // return <p>Loading, please wait!!!!</p>;
      return (
        <div className="d-flex align-items-center, justify-content-center">
          <ReactLoading
            type={"spin"}
            color={"#0000ff"}
            height={100}
            width={100}
          />
          <h4 className="pt-3 pl-3 ml-3">Loading Files!</h4>
        </div>
      );
    }

    return (
      <button
        className="btn btn-primary"
        onClick={this.downloadUserDataPortability}
      >
        Download data: data portability
      </button>
    );
  }
}
