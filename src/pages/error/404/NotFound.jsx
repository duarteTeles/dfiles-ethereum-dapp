import React, { Component } from "react";
import "./_NotFound.scss";
import NavbarItems from "../../../components/Navbar";

export default class NotFound extends Component {
  render() {
    return (
      <div>
        <NavbarItems> </NavbarItems>
        <div className="FourOhFour">
          <div
            className="bg"
            style={{ backgroundImage: "url(" + this.props.image + ")" }}
          />
          <div className="code">404</div>
        </div>
      </div>
    );
  }
}

NotFound.defaultProps = {
  image: "http://i.giphy.com/l117HrgEinjIA.gif"
};
