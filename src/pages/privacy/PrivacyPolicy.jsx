import React from "react";
import PropTypes from "prop-types";
import NavbarItems from "../../components/Navbar";

const propTypes = {};

const defaultProps = {};

export default class PrivacyPolicy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div
        className="container"
        // style={{
        //   display: "flex",
        //   alignItems: "center",
        //   justifyContent: "center"
        // }}
      >
        {/* <h1>Login</h1> */}
        <NavbarItems />
        <div className="privacyNotice">
          <h4
            className="pt-3 font-weight-bold text-center"
            style={{ fontSize: "3.4em" }}
          >
            Our Privacy Notice
          </h4>
          <div className="pt-5 privacyItems">
            <ol>
              <li className="font-weight-bold">Background</li>
              <p />
              This privacy notice let's you understand what happens to any
              personal data that is given to us, DFiles Inc, or any that we may
              collect from or about you. It applies exclusively to the DFiles
              DApp. This privacy notice applies to personal information
              processed by or on behalf of DFiles Inc.
              <p />
              <span className="font-weight-bold">
                The DFiles Inc. and our Representative
              </span>
              <p />
              We are DFiles Inc, a fictional company located in the United
              Kingdom. We are a data controller and we also process your
              personal data.
              <p />
              We do not have a{" "}
              <span className="font-weight-bold">Data Protection Officer</span>,
              due to our small size. Instead, we have Mr. John Smith, our
              representative, who is responsible for protecting your data.
              <p />
              <li>
                <span className="font-weight-bold">
                  What Kinds of Personal Information About You Do We Process?
                </span>
                <p />
                Personal information that we will process in connection with the
                DFiles DApp, if relevant, includes:
                <p />
                <ul>
                  <li>Your email address</li>
                  <li>Your Ethereum Address</li>
                  <li>Your unique username</li>
                  <li>Your hashed password</li>
                  <li>
                    A unique, auto-generated RSA private key that is used to
                    encrypt and decrypt your uploaded files to an IPFS node
                  </li>
                </ul>
              </li>
              <p />
              <li>
                <span className="font-weight-bold">
                  {" "}
                  What is the Source of Your Personal Information?
                </span>
                <p />
                We will collect personal information from the following general
                sources:
                <p />
                <ul>
                  <li>From you directly</li>
                  <li>Information generated about you when you use our DApp</li>
                </ul>
                <p />
              </li>
              <li>
                <span className="font-weight-bold">
                  What Do We Use Your Personal Data For?
                </span>
                <p />
                We use your personal data, including any of the personal data
                listed above, for the following purposes:
                <p />
                <ul>
                  <li>User login verification</li>
                  <li>User registration </li>
                  <li>Encrypting your uploaded files</li>
                  <li>Decrypting and viewing your files</li>
                  <li>Updating your information</li>
                  <li>
                    To perform and/or test the performance of DFiles and
                    internal processes
                  </li>
                  <li>
                    To follow guidance and best practice under the change to
                    rules of governmental and regulatory bodies
                  </li>
                  <li>
                    To comply with legal and regulatory obligations,
                    requirements and guidance
                  </li>
                  <p />
                </ul>
              </li>
              <li>
                <span className="font-weight-bold">
                  What Are the Legal Grounds for our Processing of Your Personal
                  Information
                </span>
                <p />
                To comply with our legal obligations With your consent or
                explicit consent:
                <ul>
                  <li>For registering your account</li>
                  <li>For encrypting your files</li>
                  <li>For decrypting your files</li>
                </ul>
                <p />
              </li>
              <li>
                <span className="font-weight-bold">
                  When Do We Share Your Personal Information with Other
                  Organisations?
                </span>
                <p />
                We currently do not share your information.
                <p />
              </li>
              <li>
                <span className="font-weight-bold">
                  How and When Can You Withdraw Your Consent?
                </span>
                <p />
                Currently, you cannot. In the future, we will be implementing
                new processes to allow consent withdrawal by contacting us.
              </li>
              <p />
              <li>
                <span className="font-weight-bold">
                  Is Your Personal Information Transferred Outside the UK?
                </span>
                <p />
                No.
              </li>
              <p />
              <li>
                <span className="font-weight-bold">
                  What Should You Do If Your Personal Information Changes?
                </span>
                <p />
                You should update your personal information in the DFiles
                account settings page.
              </li>
              <p />
              <li>
                <span className="font-weight-bold">
                  Do you have to provide your personal information to us?
                </span>
                <p />
                We cannot provide you with our DFiles features if you do not
                provide certain information to us. In cases where providing some
                personal information is optional, it will be clear.
                <p />
              </li>
              <li>
                <span className="font-weight-bold">
                  {" "}
                  For How Long Is Your Personal Information Retained by Us?
                </span>
                <p />
                Unless we explain otherwise to you, we will hold your personal
                information based on the following criteria:
                <ul>
                  <li>
                    For as long as you upload files using our platform and until
                    you delete your account
                  </li>
                  <li>
                    Retention periods in line with legal and regulatory
                    requirements or guidance
                    <p />
                  </li>
                </ul>
                <li>
                  <span className="font-weight-bold">
                    {" "}
                    What are Your Rights under Data Protection Laws?
                  </span>
                  <p />
                  Here is a list of the rights that all individuals have under
                  data protection laws. They do not apply in all circumstances:
                  <ul>
                    <li>
                      The right{" "}
                      <span className="font-weight-bold">to be informed</span>{" "}
                      about the processing of your personal information
                      <li>
                        The right to have your personal information{" "}
                        <span className="font-weight-bold">
                          corrected if it is inaccurate
                        </span>{" "}
                        and to have{" "}
                        <span className="font-weight-bold">
                          personal information information completed
                        </span>{" "}
                      </li>
                      <li>
                        The right to{" "}
                        <span className="font-weight-bold">
                          have your personal information erased
                        </span>{" "}
                        (the "right to be forgotten")
                      </li>
                      <li>
                        The right to{" "}
                        <span className="font-weight-bold">request access</span>{" "}
                        to your personal information and to obtain information
                        about how we process it
                      </li>
                      <li>
                        The right to{" "}
                        <span className="font-weight-bold">
                          move, copy or transfer your personal information
                        </span>{" "}
                        ("data portability")
                      </li>
                    </li>
                  </ul>
                </li>
              </li>
            </ol>
          </div>
        </div>
      </div>
    );
  }
}

PrivacyPolicy.propTypes = propTypes;
PrivacyPolicy.defaultProps = defaultProps;
