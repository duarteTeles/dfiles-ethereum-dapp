import React from "react";

import Dropzone from "react-dropzone";
import { Button, Form } from "react-bootstrap";
import ipfs from "../../../components/Ipfs";
import FileCommonPages from "../FilesCommonPages";
import FilesContract from "../../../../build/contracts/Files.json";

const contract = require("truffle-contract");
const filesContract = contract(FilesContract);
import getWeb3 from "../../../utils/getWeb3";
const CryptoJS = require("crypto-js");
import cookie from "react-cookies";
import { Link } from "react-router-dom";

export default class UploadFiles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dropzoneOriginalMessage: " Drop file here or click to upload (1GB max)",
      dropzoneMessage: "",
      dropzoneSuccessful: false,
      fileUploadSuccessful: false,
      file: {},
      accountFrom: null,
      accountBalanceFrom: 0,
      uploadedBlockchainFiles: [],
      web3: null,
      fileBuffer: "",
      isUploadingFiles: false,
      ipfsLocalFileLink: "",
      fileDeleted: false,
      transactionAddFileUserCanceled: null,
      userPrivateKey: null
    };
    // This binding is necessary to make `this` work in the callback
    this.handleFileDeletion = this.handleFileDeletion.bind(this);
  }

  // async checkForRepeatedFiles() {
  //   let filesContractInstance = await filesContract.deployed();
  //   let blockchainFilesObject = [];
  //   try {
  //     const filesBlockchainArrayLength = (await filesContractInstance.getUserFilesLength()).toNumber();
  //     if (filesBlockchainArrayLength === 0) {
  //       return false;
  //     }

  //     for (let index = 0; index < filesBlockchainArrayLength; index++) {
  //       const file = await filesContractInstance.getFileAtIndex(index);

  //       if (file) {
  //         blockchainFilesObject = {
  //           name: file[0],
  //           extension: file[1],
  //           metadata: file[4],
  //           size: file[2].toNumber(),
  //           hash: file[3],
  //           timestamp: file[5].toNumber()
  //         };

  //         console.log("fileeees ", blockchainFilesObject);
  //       }
  //     }
  //   } catch (error) {
  //     console.log("error: ", error);
  //   }
  // }

  async componentDidMount() {
    // get User private key from cookie
    if (cookie.load("userData") !== undefined) {
      this.setState({ userPrivateKey: cookie.load("userData").privateKey });
    }

    // Get network provider and web3 filesContract. See utils/getWeb3 for more info.
    try {
      let results = await getWeb3;
      this.setState({ web3: results.web3 });
      filesContract.setProvider(this.state.web3.currentProvider);
      // Instantiate contract once web3 provided.
    } catch (error) {
      console.log("Metamask not found.", error);
    }

    this.state.web3.eth.getAccounts(async (error, accounts) => {
      if (error) {
        console.log("wtf error", error);
      } else {
        console.log("accounts: ", accounts);
        if (accounts) {
          this.setState({
            accountFrom: accounts[0]
          });
        }
      }

      let accountFromBalance = await this.state.web3.eth.getBalance(
        accounts[0]
      );
      console.log(
        "account balance from account: " + accounts[0] + ": ",
        this.state.web3.utils.fromWei(accountFromBalance, "ether")
      );
      if (accountFromBalance) {
        this.setState({
          accountFromBalance: this.state.web3.utils.fromWei(
            accountFromBalance,
            "ether"
          )
        });
      }
    });
    // this.checkForRepeatedFiles();
  }

  handleFileDeletion(e) {
    let fileArray = [...this.state.file]; // make a separate copy of the array
    let index = fileArray.indexOf(e.target.value);
    fileArray.splice(index, 1);
    this.setState({ file: fileArray });
    this.setState({
      dropzoneMessage: this.state.dropzoneOriginalMessage
    });
    this.setState({ fileDeleted: true });
  }

  onDrop = (accepted, rejected) => {
    let acceptedFileState;
    let username = null;
    if (cookie.load("userData") !== undefined) {
      username = cookie.load("userData").username;
    }

    if (accepted === undefined || accepted.length === 0) {
      if (
        rejected["0"].name.split(".")[1] === "docx" ||
        rejected["0"].name.split(".")[1] === "pptx" ||
        rejected["0"].name.split(".")[1] === "xlsx"
      ) {
        acceptedFileState = Object.assign({}, this.state);
        acceptedFileState.file.name = rejected["0"].name;
        acceptedFileState.file.hash = "0x00000";
        acceptedFileState.file.extension = rejected["0"].name.split(".")[1];
        // acceptedFileState.file.metadata = "";
        acceptedFileState.file.size = rejected["0"].size;
        acceptedFileState.file.timestamp = Date.now().toString();
        acceptedFileState.file.ethereumAccount = this.state.accountFrom;

        this.setState({ dropzoneSuccessful: true });

        this.setState(acceptedFileState);

        this.setState({ dropzoneMessage: this.state.file.name });

        let reader = new window.FileReader();
        reader.readAsArrayBuffer(rejected[0]);
        reader.onloadend = () => this.convertToBuffer(reader);
      } else {
        console.log("rejected ", rejected);
      }
    } else {
      // this.checkForRepeatedFiles(
      //   accepted["0"].name,
      //   accepted["0"].name.split(".")[1],
      //   accepted["0"].size
      // ).then(fileIsRepeatedOrNot => {

      acceptedFileState = Object.assign({}, this.state);
      acceptedFileState.file.name = accepted["0"].name;
      acceptedFileState.file.hash = "0x00000";
      acceptedFileState.file.extension = accepted["0"].name.split(".")[1];
      // acceptedFileState.file.metadata = "";
      acceptedFileState.file.size = accepted["0"].size;
      acceptedFileState.file.timestamp = Date.now().toString();
      acceptedFileState.file.ethereumAccount = this.state.accountFrom;

      console.log("I was accepted! ", accepted["0"].name);

      this.setState({ dropzoneSuccessful: true });

      this.setState(acceptedFileState);

      this.setState({ dropzoneMessage: this.state.file.name });
      console.log("accepted file 0", accepted[0]);
      let reader = new window.FileReader();
      reader.readAsArrayBuffer(accepted[0]);
      reader.onloadend = () => this.convertToBuffer(reader);
    }
  };

  convertToBuffer = async reader => {
    //file is converted to a buffer for upload to IPFS
    const buffer = await Buffer.from(reader.result);
    //set this buffer using es6 syntax
    // TODO: Remove unencrypted buffer
    this.setState({ buffer });
    console.log("buffer ", buffer);

    let encryptedBufferTemp;
    let encryptedBuffer;
    // Encrypt
    try {
      encryptedBufferTemp = CryptoJS.AES.encrypt(
        JSON.stringify(buffer),
        this.state.userPrivateKey.toString()
      );
      // Create a buffer from encrypted string (other buffer)
      encryptedBuffer = await Buffer.from(encryptedBufferTemp.toString());

      this.setState({ encryptedBuffer });
      this.setState({ encryptedBufferTemp });
      console.log("cipher text ", this.state.encryptedBufferTemp.toString());
      console.log("buffer1 ", encryptedBuffer);
    } catch (e) {
      const msg = "Can't encrypt: INVALID: Crypto.AES.encrypt: " + e.message;
      //alert(msg);
      throw msg;
    }
    console.log("encrypted Buffer ", this.state.encryptedBuffer);
    //   // Decrypt
    //   try {
    //     let bytes = CryptoJS.AES.decrypt(
    //       encryptedBuffer.toString(),
    //       this.state.userPrivateKey.toString()
    //     );
    //     let plaintext = bytes.toString(CryptoJS.enc.Utf8);
    //     console.log("plaintext ", Buffer.from(JSON.parse(plaintext).data));
    //   } catch (e) {
    //     var msg = "Can't decrypt: wrong cipher";
    //     console.log("error ", msg);
    //     throw msg;
    //   }
    // };
  };
  handleSubmit = async event => {
    console.log("upload button clicked!");
    event.preventDefault();
    this.setState({ transactionAddFileUserCanceled: true });

    let file = this.state.file;

    await ipfs.add(this.state.encryptedBuffer, (err, ipfsHash) => {
      if (!err) {
        console.log("encrypted buffer ", this.state.encryptedBuffer);
        //TODO: Add error handling
        file.hash = ipfsHash[0].hash;
        this.setState({
          ipfsLocalFileLink: `http://localhost:8080/ipfs/${ipfsHash[0].hash}`,
          fileDeleted: false
        });
        console.log("acc ", this.state.accountFrom);

        this.setState({ isUploadingFiles: true });
        // filesContract
        //   .deployed()
        filesContract
          .at("0x09C5b627354eF29c3B6507aABa7386Ce240Fff9C")
          .then(filesContractInstance => {
            // let transactionHash;
            if (filesContractInstance) {
              try {
                filesContractInstance
                  .addFile(
                    file.name,
                    file.hash,
                    file.extension,
                    file.size,
                    file.timestamp,
                    file.ethereumAccount,
                    {
                      from: this.state.accountFrom
                    }
                  )
                  .then(transactionHash => {
                    // try {
                    //   await filesContractInstance.addTxHashes(transactionHash.tx, {
                    //     from: this.state.accountFrom
                    //   });
                    // } catch (error) {
                    //   console.log(error);
                    // }
                    console.log("file ", file);
                    this.setState({
                      transactionAddFileUserCanceled: false

                      // file: []
                    });
                  })
                  //TODO: Better error handling
                  .catch(e => {
                    // alert("User canceled transaction!!! ", e);
                    console.log("canceled ", e);

                    this.setState({ file: [] });
                  });
              } catch (error) {
                alert("user canceled transaction");
                console.log("error ", error);
              }
            }
            this.setState({ isLoadingUploadFile: false });
          })
          .catch(function(e) {
            console.log("transaction canceled! ", e);
          });
      }

      console.log("err ", err, ipfsHash);
    }); //onSubmit
  };
  render() {
    if (cookie.load("userData") === undefined) {
      return (
        <div>
          <FileCommonPages>
            <h4>
              Please{" "}
              <Link to={`/auth/login`}>
                <strong>Login </strong>
              </Link>{" "}
              to view this page.
            </h4>
          </FileCommonPages>
        </div>
      );
    }

    return (
      <div>
        <FileCommonPages>
          <div id="uploadFilesIntro">
            <div className="container-fluid">
              <div
                id="uploadFilesHeader"
                className="row text-center black-text"
              >
                <div className="col-md-12">
                  <h2 className="font-weight-bold mb-2">
                    {/* TODO: Missing Logo topping this */}
                    {/* // id="cover-main-header" Upload Your Most Important */}
                    Upload Documents
                  </h2>
                  <hr className="hr-light" />
                  <p className="my-2">
                    One-click Upload of a document or an important file.
                    Supported formats:
                    <em>
                      png, txt, jpeg, gif, bmp; pdf, csv, xlsx, pptx, docx
                    </em>
                  </p>
                  <div className="Dropzone mt-5" />
                  <Form onSubmit={this.handleSubmit}>
                    <Dropzone
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        flexDirection: "column",
                        textAlign: "center",
                        height: 200,
                        borderWidth: 2,
                        borderColor: "rgb(102, 102, 102)",
                        borderStyle: "dashed",
                        borderRadius: 5
                      }}
                      multiple={false}
                      maxSize={1073741824}
                      accept="image/jpeg, image/png, image/gif, image/bmp, application/pdf, text/csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.openxmlformats-officedocument.presentationml.presentation"
                      onDrop={
                        this.onDrop // accept="image/jpeg, image/png, image/gif, image/bmp, application/pdf, text/csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.openxmlformats-officedocument.presentationml.presentation" //1GB
                      }
                      disableClick={
                        Object.keys(this.state.file).length > 0 ? true : false
                      }
                    >
                      {Object.keys(this.state.file).length > 0 ? (
                        <ul>
                          <li>
                            {this.state.dropzoneMessage}{" "}
                            <Button bsStyle="primary" type="submit">
                              <i className="fas fa-upload" /> Upload File
                            </Button>
                            <Button onClick={this.handleFileDeletion}>
                              <i className="fas fa-trash-alt fa-2x" /> Delete
                              File
                            </Button>
                          </li>
                        </ul>
                      ) : (
                        this.state.dropzoneOriginalMessage
                      )}
                    </Dropzone>
                  </Form>{" "}
                  {this.state.ipfsLocalFileLink &&
                  !this.state.fileDeleted &&
                  this.state.transactionAddFileUserCanceled === false ? (
                    <div className="ipfsFileUploadSuccessful">
                      <h4>
                        {console.log("this.state.file", this.state.file.name)}
                        File {this.state.file.name} upload successful. View it
                        in myfiles or{" "}
                        <a href={this.state.ipfsLocalFileLink} target="_blank">
                          click this link
                        </a>{" "}
                      </h4>
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
          {/* <div className="container-fluid"> */}
          {/* <h4>
            <FilesSmartContract />
          </h4> */}
          {/* <div className="row">
            <div className="col-md-2 mt-5">
              <p />My Files
              <p>Shared/Transfered Files</p>
              <hr />
              <p className="mt-3"> My Ethereum Account</p>
            </div>
            <div className="col-md-8">GG2</div>
            <div className="col-md-2">GG3</div>
          </div> */}
        </FileCommonPages>
      </div>
    );
  }
}
