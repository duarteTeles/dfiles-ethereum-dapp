import React, { Component } from "react";
import ipfs from "../../../components/Ipfs";
import FilesCommonPages from "../FilesCommonPages";
import Paper from "@material-ui/core/Paper";
import Iframe from "react-iframe";
import CryptoJS from "crypto-js";
import cookie from "react-cookies";
import fileDownloader from "js-file-download";
import ReactNotification from "react-notifications-component";
import { Link } from "react-router-dom";
import ReactLoading from "react-loading";

// import ReactTooltip from "react-tooltip";
import {
  SelectionState,
  IntegratedSelection,
  SearchState,
  IntegratedFiltering,
  SortingState,
  IntegratedSorting
} from "@devexpress/dx-react-grid";
import {
  Grid,
  VirtualTable,
  TableHeaderRow,
  TableSelection,
  Toolbar,
  SearchPanel,
  ColumnChooser,
  TableColumnVisibility,
  TableColumnResizing,
  TableRowDetail
} from "@devexpress/dx-react-grid-material-ui";

import FilesContract from "../../../../build/contracts/Files.json";
const contract = require("truffle-contract");
const filesContract = contract(FilesContract);
import getWeb3 from "../../../utils/getWeb3";
import { formatBytes } from "../../../utils/UtilFunctions";
import ReactIframeResizer from "react-iframe-resizer-super";

const ipfsFileUrl = `https://gateway.ipfs.io/ipfs/`;
const _ = require("underscore");

import "./_myFiles.scss";

const iframeResizerOptions = { checkOrigin: false };

const getMimeType = extension => {
  console.log("called!!!!");
  // png, jpeg, jpg, gif, bmp; pdf, csv, xslx, pptx, docx
  const mimeTypes = {
    png: "image/png",
    jpeg: "image/jpg",
    jpg: "image/jpg",
    gif: "image/gif",
    bmp: "image/bmp",
    pdf: "application/pdf",
    csv: "text/csv",
    xlsx: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    pptx:
      "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    docx:
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
  };

  console.log("called!!!!", Object.keys(mimeTypes));

  if (_.propertyOf(mimeTypes)(extension)) {
    console.log(" here bitch!!!! ", _.propertyOf(mimeTypes)(extension));
    return _.propertyOf(mimeTypes)(extension);
  } else {
    return undefined;
  }
};

const Cell = props => (
  <VirtualTable.Cell
    {...props}
    style={
      {
        // paddingTop: 0,
        // paddingBottom: 0
      }
    }
  />
);

const Row = props => <VirtualTable.Row {...props} style={{ height: 200 }} />;

export default class MyFiles extends Component {
  constructor(props) {
    super(props);
    this.addNotification = this.addNotification.bind(this);
    this.notificationDOMRef = React.createRef();

    this.readAllStoredFilesInBlockchain = this.readAllStoredFilesInBlockchain.bind(
      this
    );
    this.downloadFile = this.downloadFile.bind(this);
    this.state = {
      accountFrom: null,
      userPrivateKey: null,
      isUserLoggedIn: false,
      //TODO: Implement Blockchain Fetching of files
      columns: [
        { name: "name", title: "Name" },
        { name: "hash", title: "Encrypted Hash" },
        { name: "url", title: "Encrypted URL" },
        { name: "decryptAndDownload", title: "Decrypt and Download File" },
        { name: "extension", title: "Extension" },
        { name: "size", title: "Size" },
        { name: "timestamp", title: "Timestamp" }
        // { name: "preview", title: "Preview" }
      ],
      defaultColumnWidths: [
        { columnName: "name", width: 100 },
        { columnName: "hash", width: 140 },
        { columnName: "url", width: 140 },
        { columnName: "decryptAndDownload", width: 200 },
        { columnName: "extension", width: 100 },
        { columnName: "size", width: 80 },
        { columnName: "timestamp", width: 120, height: 240 }
        // { columnName: "preview", width: 240 }
      ],
      rows: [],
      decryptedBuffer: "",
      selection: [],
      defaultHiddenColumnNames: ["timestamp"],
      web3: null,
      isFetchingFiles: true
    };

    this.changeSelection = selection => this.setState({ selection });
  }

  addNotification() {
    this.notificationDOMRef.current.addNotification({
      title: "Decryption Failed",
      message: "File decryption filed, wrong decryption key",
      type: "danger",
      insert: "top",
      container: "top-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: { duration: 5000 },
      dismissable: { click: true }
    });
  }

  downloadFile(fileName, fileExtension, encryptedFileHash) {
    // console.log("gg ", this.state.decryptedBuffer);
    const userPrivateKey = this.state.userPrivateKey;
    // console.log("wtf ", userPrivateKey);

    // ipfs.files.get(validIPFSHash, function(err, files) {
    console.log("encrypted file hash ", encryptedFileHash);
    ipfs.files.get(encryptedFileHash, (err, files) => {
      files.forEach(file => {
        console.log(file.path);
        console.log("File content >> ", file.content);
        console.log("private key ", userPrivateKey);
        // Decrypt
        try {
          let bytes = CryptoJS.AES.decrypt(
            file.content.toString(),
            userPrivateKey
          );
          let decryptedBuffer = bytes.toString(CryptoJS.enc.Utf8);
          let decryptedBufferFull = Buffer.from(
            JSON.parse(decryptedBuffer).data
          );
          // this.setState({
          //   decryptedBuffer: decryptedBufferFull
          // });
          console.log("decryptedBuffer ", decryptedBufferFull);
          this.setState({ decryptedBuffer: decryptedBufferFull });
          fileDownloader(this.state.decryptedBuffer, `${fileName}`);
          // this.setState = { decryptedBuffer: decryptedBuffer };
        } catch (e) {
          let msg = "Can't decrypt: wrong cipher " + e;
          this.addNotification();
          console.log("error ", msg);
          // throw msg;
        }
        // if (decryptedBufferFull) {
        //   this.setState({ decryptedBuffer: "gg" }, () =>
        //     console.log(this.state.decryptedBuffer)
        //   );
        //   // console.log("asdasdadas ", this.state);
        //   console.log("ggg ", decryptedBufferFull);
        // }
      });
    });
  }

  // renderFilePreview(fileExtension, fileUrl, fileName) {
  //   if (fileExtension === "csv") {
  //     return (
  //       <a
  //         href={fileUrl}
  //         download={`${fileName}.${fileExtension}`}
  //         type="text/csv"
  //       >
  //         >{" "}
  //         <img
  //           src="https://cdn2.iconfinder.com/data/icons/file-formats-4-1/100/file_formats_4_csv-512.png"
  //           height="150"
  //           width="150"
  //         />
  //       </a>
  //     );
  //   } else if (fileExtension === "xlsx") {
  //     return (
  //       <a href={fileUrl} download={`${fileName}.${fileExtension}`}>
  //         {" "}
  //         <img
  //           src="https://cdn2.iconfinder.com/data/icons/file-format-colorful/100/xlsx-512.png"
  //           height="150"
  //           width="150"
  //         />
  //       </a>
  //     );
  //   } else if (fileExtension === "pptx") {
  //     return (
  //       <a href={fileUrl} download={`${fileName}.${fileExtension}`}>
  //         {" "}
  //         <img
  //           src="http://icons.iconarchive.com/icons/trayse101/basic-filetypes-2/256/pptx-icon.png"
  //           height="100"
  //           width="100"
  //         />
  //       </a>
  //     );
  //   } else if (fileExtension === "docx") {
  //     return (
  //       <a href={fileUrl} download={`${fileName}.${fileExtension}`}>
  //         {" "}
  //         <img
  //           src="https://cdn2.iconfinder.com/data/icons/file-formats-3-1/100/file_formats3_docx-512.png"
  //           height="100"
  //           width="100"
  //         />
  //       </a>
  //     );
  //   } else {
  //     //TODO: make embeded pdf clickable...
  //     let mimeType = getMimeType(fileExtension);
  //     return (
  //       <div>
  //         <a href={fileUrl}>
  //           <object
  //             style={{ overflow: "hidden" }}
  //             data={fileUrl}
  //             type={mimeType}
  //             width="150"
  //             height="150"
  //           />
  //         </a>
  //       </div>
  //     );
  //   }
  // }

  async readAllStoredFilesInBlockchain() {
    // let filesContractInstance = await filesContract.deployed();
    let filesContractInstance = await filesContract.at(
      "0x37b143616dA99BB2A9bC6F2b01eD4F0872343654"
    );
    // If using web3 straight away, we need the abi

    // let filesContractInstance = await filesContract.at(
    //   "0x64F18cC73412d5809958256dAd881E4A9B06134A"
    // );

    let fileObject = [];
    let decryptedBufferFull = null;
    if (this.state.accountFrom !== undefined) {
      try {
        const filesBlockchainArrayLength = (await filesContractInstance.getUserFilesLength()).toNumber();
        if (filesBlockchainArrayLength === 0) {
          console.log("false");
        }

        let newFilesStateArray = this.state.rows.slice(); //copy the array
        // let mimeType = "";
        for (let index = 0; index < filesBlockchainArrayLength; index++) {
          const file = await filesContractInstance.getFileAtIndex(index);
          if (file && this.state.accountFrom === file[5]) {
            console.log("file ", file);
            console.log("index ", index);
            fileObject = {
              // name: (

              //     {/* <a
              //       // data-tip
              //       // data-for={`global${index}`}
              //       href={`${ipfsFileUrl}${file[3]}`}
              //       target="_blank"
              //     > */}
              //     {/* </a> */}
              //     file[0]

              // ),

              // @ts-ignore
              name: file[0],
              extension: file[1],
              url: `${ipfsFileUrl}${file[3]}`,
              decryptAndDownload: (
                <button
                  className={"btn btn-lg btn-primary"}
                  onClick={() => this.downloadFile(file[0], file[1], file[3])}
                >
                  Decrypt and Download
                </button>
              ),
              // metadata: file[4],
              size: formatBytes(file[2].toNumber(), 2),
              hash: file[3],
              timestamp: file[4],
              ethereumAccount: file[5]
            };

            newFilesStateArray.push(fileObject);
          }
        }

        // if (cookie.load("userData").username === rows.username} )
        this.setState({ rows: newFilesStateArray });

        this.setState({ isFetchingFiles: false });
      } catch (error) {
        console.log("error: ", error);
      }
    }
  }

  async componentDidMount() {
    // get User private key from cookie
    if (cookie.load("userData") !== undefined) {
      this.setState({ userPrivateKey: cookie.load("userData").privateKey });
    }
    // Get network provider and web3 filesContract. See utils/getWeb3 for more info.
    try {
      let results = await getWeb3;
      this.setState({ web3: results.web3 });
      filesContract.setProvider(this.state.web3.currentProvider);
      // Instantiate contract once web3 provided.
    } catch (error) {
      console.log("Metamask not found.", error);
    }

    this.state.web3.eth.getAccounts(async (error, accounts) => {
      if (error) {
        console.log("wtf error", error);
      } else {
        console.log("accounts: ", accounts);
        if (accounts) {
          this.setState({
            accountFrom: accounts[0].toLowerCase()
          });
        }
      }
    });

    this.readAllStoredFilesInBlockchain();
  }

  // tooltips() {
  //   for (let i = 0; i < this.state.rows.length; i++) {
  //     {
  //       console.log("url ", this.state.rows[1].url);
  //     }
  //     return (
  //       <ReactTooltip id={`global${i}`} aria-haspopup="true">
  //         <img
  //           src={this.state.rows[1].url}
  //           style={{ width: 500, height: 500 }}
  //         />
  //       </ReactTooltip>
  //     );
  //   }
  // }

  render() {
    if (cookie.load("userData") === undefined) {
      return (
        <div>
          <FilesCommonPages>
            <h4>
              Please{" "}
              <Link to={`/auth/login`}>
                {" "}
                <strong>Login </strong>
              </Link>
              to view this page.
            </h4>
          </FilesCommonPages>
        </div>
      );
    }

    const {
      rows,
      columns,
      selection,
      defaultHiddenColumnNames,
      defaultColumnWidths
    } = this.state;

    if (
      this.state.isFetchingFiles &&
      !this.state.row &&
      this.state.accountFrom !== undefined
    ) {
      // return <p>Loading, please wait!!!!</p>;
      return (
        <div className="d-flex align-items-center, justify-content-center">
          <ReactLoading
            type={"spin"}
            color={"#0000ff"}
            height={100}
            width={100}
          />
          <h4 className="pt-3 pl-3 ml-3">Loading Files!</h4>
        </div>
      );
    } else {
      let selectedRows = [];
      for (let i = 0; i < this.state.selection.sort().length; i++) {
        selectedRows.push(rows[this.state.selection[i]]);
      }

      return (
        <div>
          <FilesCommonPages>
            {this.state.rows.length > 0 ? (
              <React.Fragment>
                {/* {this.tooltips()} */}
                <ReactNotification ref={this.notificationDOMRef} />

                {/* <span>Total rows selected: {selection.length}</span>

                <p /> */}
                {/* // TODO: add Batch support for selecting files! */}
                {/* <span>Selected Row: {JSON.stringify(selectedRows)}</span>
                <span>Selection State: {this.state.selection}</span> */}
                <Paper>
                  <Grid rows={rows} columns={columns}>
                    <SelectionState
                      selection={selection}
                      onSelectionChange={this.changeSelection}
                    />
                    <SortingState
                      defaultSorting={[
                        { columnName: "name", direction: "asc" }
                      ]}
                    />
                    <IntegratedSorting />

                    <IntegratedSelection />
                    <SearchState />
                    <IntegratedFiltering />
                    <VirtualTable
                      estimatedRowHeight={200}
                      rowComponent={Row}
                      cellComponent={Cell}
                    />
                    <TableColumnResizing
                      defaultColumnWidths={defaultColumnWidths}
                    />
                    <TableHeaderRow showSortingControls />
                    <TableColumnVisibility
                      defaultHiddenColumnNames={defaultHiddenColumnNames}
                    />
                    <TableSelection showSelectAll />
                    <Toolbar />
                    <ColumnChooser />
                    <SearchPanel />
                  </Grid>
                </Paper>
              </React.Fragment>
            ) : (
              <h1>
                No files available.{" "}
                <Link to={`/files/upload-files`}>Start Uploading!</Link>
              </h1>
            )}
          </FilesCommonPages>
        </div>
      );
    }
  }
}
