import React, { Component } from "react";
import ipfs from "../../../components/Ipfs";
import FilesCommonPages from "../FilesCommonPages";
import Paper from "@material-ui/core/Paper";

import CryptoJS from "crypto-js";
import cookie from "react-cookies";
import fileDownloader from "js-file-download";
import ReactNotification from "react-notifications-component";

import { Link } from "react-router-dom";

// import ReactTooltip from "react-tooltip";
import {
  SelectionState,
  IntegratedSelection,
  SearchState,
  IntegratedFiltering,
  SortingState,
  IntegratedSorting
} from "@devexpress/dx-react-grid";
import {
  Grid,
  VirtualTable,
  TableHeaderRow,
  TableSelection,
  Toolbar,
  SearchPanel,
  ColumnChooser,
  TableColumnVisibility,
  TableColumnResizing,
  TableRowDetail
} from "@devexpress/dx-react-grid-material-ui";

const fs = require("fs");
// import FilesContract from "../../../../build/contracts/Files.json";
const FilesContract = [
  {
    constant: true,
    inputs: [],
    name: "owner",
    outputs: [{ name: "", type: "address" }],
    payable: false,
    stateMutability: "view",
    type: "function"
  },
  {
    constant: false,
    inputs: [
      { name: "_name", type: "string" },
      { name: "_hash", type: "string" },
      { name: "_extension", type: "string" },
      { name: "_size", type: "uint32" },
      { name: "_timestamp", type: "string" },
      { name: "_ethereumAccount", type: "string" }
    ],
    name: "addFile",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    constant: true,
    inputs: [{ name: "index", type: "uint256" }],
    name: "getFileAtIndex",
    outputs: [
      { name: "", type: "string" },
      { name: "", type: "string" },
      { name: "", type: "uint32" },
      { name: "", type: "string" },
      { name: "", type: "string" },
      { name: "", type: "string" }
    ],
    payable: false,
    stateMutability: "view",
    type: "function"
  },
  {
    constant: true,
    inputs: [],
    name: "getUserFilesLength",
    outputs: [{ name: "", type: "uint256" }],
    payable: false,
    stateMutability: "view",
    type: "function"
  },
  {
    constant: false,
    inputs: [{ name: "newOwner", type: "address" }],
    name: "transferOwnership",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    inputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "constructor"
  },
  {
    anonymous: false,
    inputs: [
      { indexed: false, name: "_name", type: "string" },
      { indexed: false, name: "_hash", type: "string" },
      { indexed: false, name: "_extension", type: "string" },
      { indexed: false, name: "_size", type: "uint32" },
      { indexed: false, name: "_timestamp", type: "string" },
      { indexed: false, name: "_ethereumAccount", type: "string" }
    ],
    name: "AddFile",
    type: "event"
  }
];
const contract = require("truffle-contract");
const filesContract = contract(FilesContract);
import getWeb3 from "../../../utils/getWeb3";
import { formatBytes } from "../../../utils/UtilFunctions";

const ipfsFileUrl = `http://localhost:8080/ipfs/`;
const _ = require("underscore");

import "./_myFiles.scss";

const Cell = props => (
  <VirtualTable.Cell
    {...props}
    style={
      {
        // paddingTop: 0,
        // paddingBottom: 0
      }
    }
  />
);

const Row = props => <VirtualTable.Row {...props} style={{ height: 200 }} />;

export default class MyFiles extends Component {
  constructor(props) {
    super(props);
    this.addNotification = this.addNotification.bind(this);
    this.notificationDOMRef = React.createRef();

    this.readAllStoredFilesInBlockchain = this.readAllStoredFilesInBlockchain.bind(
      this
    );
    this.downloadFile = this.downloadFile.bind(this);
    this.state = {
      userPrivateKey: null,
      isUserLoggedIn: false,
      //TODO: Implement Blockchain Fetching of files
      columns: [
        { name: "name", title: "Name" },
        { name: "hash", title: "Encrypted Hash" },
        { name: "url", title: "Encrypted URL" },
        { name: "decryptAndDownload", title: "Decrypt and Download File" },
        { name: "extension", title: "Extension" },
        { name: "size", title: "Size" },
        { name: "timestamp", title: "Timestamp" }
        // { name: "preview", title: "Preview" }
      ],
      defaultColumnWidths: [
        { columnName: "name", width: 100 },
        { columnName: "hash", width: 140 },
        { columnName: "url", width: 140 },
        { columnName: "decryptAndDownload", width: 200 },
        { columnName: "extension", width: 100 },
        { columnName: "size", width: 80 },
        { columnName: "timestamp", width: 120, height: 240 }
        // { columnName: "preview", width: 240 }
      ],
      rows: [],
      decryptedBuffer: "",
      selection: [],
      defaultHiddenColumnNames: ["timestamp"],
      web3: null,
      isFetchingFiles: true
    };

    this.changeSelection = selection => this.setState({ selection });
  }

  addNotification() {
    this.notificationDOMRef.current.addNotification({
      title: "Decryption Failed",
      message: "File decryption filed, wrong decryption key",
      type: "danger",
      insert: "top",
      container: "top-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: { duration: 5000 },
      dismissable: { click: true }
    });
  }

  downloadFile(fileName, fileExtension, encryptedFileHash) {
    // console.log("gg ", this.state.decryptedBuffer);
    const userPrivateKey = this.state.userPrivateKey;
    // console.log("wtf ", userPrivateKey);

    // ipfs.files.get(validIPFSHash, function(err, files) {
    console.log("encrypted file hash ", encryptedFileHash);
    ipfs.files.get(encryptedFileHash, (err, files) => {
      files.forEach(file => {
        console.log(file.path);
        console.log("File content >> ", file.content);

        // Decrypt
        try {
          let bytes = CryptoJS.AES.decrypt(
            file.content.toString(),
            userPrivateKey
          );
          let decryptedBuffer = bytes.toString(CryptoJS.enc.Utf8);
          let decryptedBufferFull = Buffer.from(
            JSON.parse(decryptedBuffer).data
          );

          console.log("decryptedBuffer ", decryptedBufferFull);
          this.setState({ decryptedBuffer: decryptedBufferFull });
          fileDownloader(this.state.decryptedBuffer, `${fileName}`);
          // this.setState = { decryptedBuffer: decryptedBuffer };
        } catch (e) {
          let msg = "Can't decrypt: wrong cipher " + e;
          this.addNotification();
          console.log("error ", msg);
          // throw msg;
        }
      });
    });
  }

  async readAllStoredFilesInBlockchain() {
    let filesContractInstance = await filesContract.deployed();
    // If using web3 straight away, we need the abi

    // let filesContractInstance = await filesContract.at(
    //   "0x64F18cC73412d5809958256dAd881E4A9B06134A"
    // );

    let fileObject = [];
    let decryptedBufferFull = null;
    try {
      const filesBlockchainArrayLength = (await filesContractInstance.getUserFilesLength()).toNumber();
      if (filesBlockchainArrayLength === 0) {
        console.log("false");
      }

      let newFilesStateArray = this.state.rows.slice(); //copy the array
      // let mimeType = "";
      for (let index = 0; index < filesBlockchainArrayLength; index++) {
        const file = await filesContractInstance.getFileAtIndex(index);

        if (file) {
          console.log("file ", file);
          console.log("index ", index);
          fileObject = {
            // @ts-ignore
            name: file[0],
            extension: file[1],
            url: `${ipfsFileUrl}${file[3]}`,
            decryptAndDownload: (
              <button
                className={"btn btn-lg btn-primary"}
                onClick={() => this.downloadFile(file[0], file[1], file[3])}
              >
                Decrypt and Download
              </button>
            ),
            // metadata: file[4],
            size: formatBytes(file[2].toNumber(), 2),
            hash: file[3],
            timestamp: file[4],
            ethereumAccount: file[5]
          };

          newFilesStateArray.push(fileObject);
        }
      }

      // Delete files from the files array if they belong to another user.
      for (let index = 0; index < newFilesStateArray.length; index++) {
        // if (
        //   cookie.load("userData").username !==
        //   newFilesStateArray[index].username
        // ) {
        //   newFilesStateArray.splice(index, 1);
        // }
        console.log("fileeeee ", newFilesStateArray[index]);
      }

      // if (cookie.load("userData").username === rows.username} )
      this.setState({ rows: newFilesStateArray });

      this.setState({ isFetchingFiles: false });
    } catch (error) {
      console.log("error: ", error);
    }
  }

  async componentDidMount() {
    // get User private key from cookie
    if (cookie.load("userData") !== undefined) {
      this.setState({ userPrivateKey: cookie.load("userData").privateKey });
    }
    // Get network provider and web3 filesContract. See utils/getWeb3 for more info.
    try {
      let results = await getWeb3;
      this.setState({ web3: results.web3 });
      filesContract.setProvider(this.state.web3.currentProvider);
      // Instantiate contract once web3 provided.
    } catch (error) {
      console.log("Metamask not found.", error);
    }

    this.readAllStoredFilesInBlockchain();
  }

  render() {
    if (cookie.load("userData") === undefined) {
      return (
        <div>
          <FilesCommonPages>
            <h4>
              Please{" "}
              <Link to={`/auth/login`}>
                <strong>Login </strong>
              </Link>{" "}
              to view this page.
            </h4>
          </FilesCommonPages>
        </div>
      );
    }

    const {
      rows,
      columns,
      selection,
      defaultHiddenColumnNames,
      defaultColumnWidths
    } = this.state;

    if (this.state.isFetchingFiles && !this.state.row) {
      return <p>Loading, please wait!!!!</p>;
    } else {
      let selectedRows = [];
      for (let i = 0; i < this.state.selection.sort().length; i++) {
        selectedRows.push(rows[this.state.selection[i]]);
      }

      return (
        <div>
          <FilesCommonPages>
            {this.state.rows.length > 0 ? (
              <React.Fragment>
                {/* {this.tooltips()} */}
                <ReactNotification ref={this.notificationDOMRef} />

                <Paper>
                  <Grid rows={rows} columns={columns}>
                    <SelectionState
                      selection={selection}
                      onSelectionChange={this.changeSelection}
                    />
                    <SortingState
                      defaultSorting={[
                        { columnName: "name", direction: "asc" }
                      ]}
                    />
                    <IntegratedSorting />

                    <IntegratedSelection />
                    <SearchState />
                    <IntegratedFiltering />
                    <VirtualTable
                      estimatedRowHeight={200}
                      rowComponent={Row}
                      cellComponent={Cell}
                    />
                    <TableColumnResizing
                      defaultColumnWidths={defaultColumnWidths}
                    />
                    <TableHeaderRow showSortingControls />
                    <TableColumnVisibility
                      defaultHiddenColumnNames={defaultHiddenColumnNames}
                    />
                    <TableSelection showSelectAll />
                    <Toolbar />
                    <ColumnChooser />
                    <SearchPanel />
                  </Grid>
                </Paper>
              </React.Fragment>
            ) : (
              <h1>
                No files available.{" "}
                <a href="/files/upload-files">Start Uploading!</a>
              </h1>
            )}
          </FilesCommonPages>
        </div>
      );
    }
  }
}
