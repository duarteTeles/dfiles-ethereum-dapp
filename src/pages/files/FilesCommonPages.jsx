import React from "react";
import NavbarItems from "../../components/Navbar";
import { Link } from "react-router-dom";

// import styles
import "./_filesCommonPages.scss";

const FilesCommonPages = props => {
  return (
    <div className="container-fluid h-100">
      <NavbarItems />
      <div className="row h-100">
        <div id="leftSideBar" className="col-md-2">
          <p style={{ marginTop: 100 }}>
            <Link to={`/files/upload-files/`}>Upload</Link>
          </p>

          <p>
            <Link to={`/files/my-files/`}>My Files</Link>
          </p>

          {/* <p>
            <Link to={`/files/sharing/`}>My Account</Link>
          </p> */}

          {/* <hr /> */}
          <p className="/account">
            {" "}
            <Link to={`/auth/account/`}>My Account</Link>
          </p>
        </div>
        <div className="col-md-8" style={{ marginTop: 100 }}>
          {/* <UploadFiles /> */}

          {props.children}
        </div>
        <div className="col-md-2" />
      </div>
    </div>
  );
};

export default FilesCommonPages;
