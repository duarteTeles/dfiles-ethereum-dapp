import React, { Component } from "react";


//navbar

// import Home from "./pages/home/Home";

import Routes from "./Routes";

// Styles
// Bootstrap
import "./styles/scss/bootstrap/bootstrap.scss";
import "./styles/scss/fonts/open-sans/open-sans.scss";
import "./styles/scss/mdboostrap/mdb.min.scss";
import "./styles/scss/fonts/oswald/oswald.scss";
import "./styles/scss/other/pure-min.scss";



class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Routes />

      /* https://medium.com/@tyler.ferguson/porting-a-multiple-page-application-to-react-a-piecemeal-approach-with-react-router-4-6285024dfcb2
    https://codesandbox.io/s/vVoQVk78
     <div className="App">
       <nav className="navbar pure-menu pure-menu-horizontal">
       </nav>

      <main className="container">
        <div className="pure-g">
          <div className="pure-u-1-1">
            <h1>Good to Go!</h1>
            <h1>YOHOOOOO</h1>
            <p>Your Truffle Box is installed and ready.</p>
            <h2>Smart Contract Example</h2>
            <p>If your contracts compiled and migrated successfully, below will show a stored value of 5 (by default).</p>
            <p>Try changing the value stored on <strong>line 59</strong> of App.js.</p>
            <p>The stored value is: {this.state.storageValue}</p>
          </div>
        </div>
      </main>
    </div>
     <Route exact path="/" component={HomePage}/>  */
    );
  }
}

export default App;
