const express = require('express');
const bodyParser = require('body-parser');
const displayRoutes = require('express-routemap');


// create express app
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}))



// parse requests of content-type - application/json
app.use(bodyParser.json())

// Configuring the database
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
        useNewUrlParser: true
    })
    .then(() => {
        console.log("Successfully connected to the database");
    }).catch(err => {
        console.log('Could not connect to the database. Exiting now...');
        process.exit();
    });

// define a simple route
app.get('/', (req, res) => {
    res.json({
        "message": "Welcome to the user authentication service for the DFiles DApp"
    });
});

// Require Users routes
require('./app/routes/user.routes.js')(app);

// Require Files routes
require('./app/routes/file.routes.js')(app);



// listen for requests
app.listen(3005, () => {
    console.log("Server is listening on port 3005");
    // or use like this
    displayRoutes(app, 'route-table.log');
});