const express = require('express');
const userRouter = express.Router();

module.exports = (app) => {

    const users = require('../controllers/user.controller.js');

    app.use(function (req, res, next) {
       res.header("Access-Control-Allow-Origin", "*");
       res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
       res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
          next();
    });

    // Create a new User
    userRouter.route('/').post(users.create);

    // Retrieve all Users
    userRouter.route('/').get(users.findAll);

    // Login a user by supplying his username and password
    // app.get('/users/login?username=:username&password=:password', users.login);
    userRouter.route('/login').get(users.login);

    userRouter.route('/userByEthereumAddress').get(users.findUserByEthereumAddress);

    // userRouter.route('/login2').get(function (req, res) {
    //     res.send("2222");
    // });

    // Retrieve a single User with username
    userRouter.route('/:username').get(users.findOne);


    // Update a User with username
    userRouter.route('/:username').put(users.update);

    // Delete a User with username
    userRouter.route('/:username').delete(users.delete);

    app.use('/users', userRouter);





}