const express = require('express');
const filesRouter = express.Router();

module.exports = (app) => {

    const files = require('../controllers/file.controller.js');

    app.use(function (req, res, next) {
        // res.header("Access-Control-Allow-Origin", "*");
        // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
       res.header("Access-Control-Allow-Origin", "*");
       res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
       res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
        next();
    });

    // Create a new User
    filesRouter.route('/').get(files.helloWorld);
    filesRouter.route('/').post(files.encrypt);



    app.use('/files', filesRouter);



}