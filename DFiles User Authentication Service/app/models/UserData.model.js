const mongoose = require('mongoose');

const UserDataSchema = mongoose.Schema({
    userData: {
        username: {
            type: String,
            index: true,
            unique: true,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        ethereumAccountAddress: {
            type: String,
            required: true
        },
        privateKey: {
            type: String,
            required: true
        },
        publicKey: {
            type: String,
            required: true
        },
        files: [{
            name: {
                type: String,
                required: true
            },
            hash: {
                type: String,
                required: true
            },
            extension: {
                type: String,
                required: true
            },
            size: {
                type: Number,
                required: true
            },
            timestamp: {
                type: String,
                required: true
            },
        }]


    },
}, {
    timestamps: false
});

module.exports = mongoose.model('UserData', UserDataSchema);