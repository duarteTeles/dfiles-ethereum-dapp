const User = require('../models/User.model');

// import bcrypt to hash password
const bcrypt = require('bcrypt');

// use 10 sault rounds
const saltRounds = 10;


// Create and Save a new User
exports.create = (req, res) => {
    // Validate request
    if (!req.body.email) {
        return res.status(400).send({
            message: "email cannot be empty"
        });
    } else if (!req.body.password) {
        return res.status(400).send({
            message: "password cannot be empty"
        });
    }

    // get user password from the request body
    const userPassword = req.body.password;

    // Create a User
    const user = new User({
        username: req.body.username,
        // firstName: req.body.firstName,
        // lastName: req.body.lastName,
        email: req.body.email,
        // store encrypted password
        password: bcrypt.hashSync(userPassword, saltRounds),
        ethereumAccountAddress: req.body.ethereumAccountAddress.toLowerCase(),
        privateKey: req.body.privateKey,
        publicKey: req.body.publicKey
    });

    // Save User in the database
    user.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the User."
            });
        });
};
// Retrieve and return all Users from the database.
exports.findAll = (req, res) => {
    User.find()
        .then(users => {
            res.send(users);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single User with username (unique)
exports.findOne = (req, res) => {
    User.findOne({
            'username': req.params.username
        })
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "User not found with username " + req.params.username
                });
            }
            res.send(user);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "User not found with username " + req.params.username
                });
            }
            return res.status(500).send({
                message: "Error retrieving user with username " + req.params.username
            });
        });
};

// Update a User identified by username in the request
exports.update = (req, res) => {

    let set = {};
    // // Validate request
    // if (!req.body.email) {
    //     return res.status(400).send({
    //         message: "email cannot be empty"
    //     });
    // } else if (!req.body.password) {
    //     return res.status(400).send({
    //         message: "password cannot be empty"
    //     });
    // }
    console.log("email ", req.body.email);
    if (req.body.email) {
        set.email = req.body.email;
    }
    if (req.body.password)
    {
        set.password = bcrypt.hashSync(req.body.password, saltRounds);
    }

    if (req.body.ethereumAccountAddress) {
        set.ethereumAccountAddress = req.body.ethereumAccountAddress;
    }
    console.log("set ", set);
    // if (req.body.firstName) {
    //     set.firstName = req.body.firstName;
    // }
    // if (req.body.lastName) {
    //     set.lastName = req.body.lastName;
    // }
    if (Object.keys(set).length === 0) {
        return res.status(400).send({
            message: "Cannot update empty fields"
        });
    }


    // Find user and update it with the request body

    User.findOneAndUpdate({
            "username": req.params.username
        }, {

            "$set":
                // "email": req.body.email,
                // "password": req.body.password,
                // ethereumAccountAddress
                // "firstName": req.body.firstName,
                // "lastName": req.body.lastName
                // // "privateKey": req.body.privateKey
                set

        }, {
            "new": true

        })
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "User not found with username " + req.params.username
                });
            }
            res.send(user);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "User not found with username " + req.params.username
                });
            }
            return res.status(500).send({
                message: "Error updating user with username " + req.params.username
            });
        });
};

// Delete a User with the specified username in the request
exports.delete = (req, res) => {
    console.log("here");
    User.findOneAndRemove({
            'username': req.params.username
        })
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "User not found with username " + req.params.username
                });
            }
            res.send({
                message: "User deleted successfully!"
            });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "User not found with username " + req.params.username
                });
            }
            return res.status(500).send({
                message: "Could not delete user with username " + req.params.username
            });
        });
};

exports.login = (req, res) => {
    const username = req.query.username;

    // user raw password
    const password = req.query.password;


    User.findOne({
            'username': username,
            // 'password': password
        })
        .then(user => {
            if (!bcrypt.compareSync(password, user.password)) {
                // if (!user) {
                return res.status(404).send({
                    message: "User or password invalid"
                });
            }
            console.log(user);

            res.send({
                username: user.username,
                // firstName: user.firstName,
                // lastName: user.lastName,
                email: user.email,
                privateKey: user.privateKey,
                publicKey: user.publicKey,
                ethereumAccountAddress: user.ethereumAccountAddress,

            });
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "User or password invalid"
                });
            }
            return res.status(500).send({
                message: "Error logging in"
            });
        });

}
exports.findUserByEthereumAddress = (req, res) => {
    const ethereumAccountAddress = req.query.ethereumAccountAddress;


    User.findOne({
            'ethereumAccountAddress': ethereumAccountAddress.toLowerCase(),
        })
        .then(user => {
            if (!user) {
                // if (!user) {
                return res.status(404).send({
                    message: "User data not found"
                });
                console.log("...", ethereumAccountAddress.toLowerCase());
            }

            res.send({
                username: user.username,
                // firstName: user.firstName,
                // lastName: user.lastName,
                email: user.email,
                privateKey: user.privateKey,
                publicKey: user.publicKey,

            });
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Ethereum account address invalid"
                });
            }
            return res.status(500).send({
                message: "Error getting user data by ethereum account address"
            });
        });

}