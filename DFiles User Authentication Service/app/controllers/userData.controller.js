const UserData = require('../models/UserData.model');


// Create and Save a new User
exports.create = (req, res) => {
    // Validate request
    if (!req.body.email) {
        return res.status(400).send({
            message: "email cannot be empty"
        });
    } else if (!req.body.password) {
        return res.status(400).send({
            message: "password cannot be empty"
        });
    }


    const userUsername = req.body.username;
    const userEmail = req.body.email;
    const userEthereumAccountAddress = req.body.ethereumAccountAddress;
    const userPrivateKey = req.body.privateKey;
    const userPublicKey = req.body.publicKey;
    const fileName = req.body.name;
    const fileHash = req.body.publicKey;
    const fileExtension = req.body.extension;
    const fileSize = req.body.publicKey;
    const fileTimestamp = req.body.timestamp;

    // Create a UserData entry so that it can be viewed later when the user invokes the data access right.

    const userData = new UserData({
        username: userUsername,
        email: userEmail,
        ethereumAccountAddress: userEthereumAccountAddress,
        privateKey: userPrivateKey,
        publicKey: userPublicKey,
        file: [{
            name: fileName,
            hash: fileHash,
            extension: fileExtension,
            size: fileSize,
            timestamp: fileTimestamp
        }]

    });

    // Save UserData in the database
    userData.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating an userData entry."
            });
        });
};
// Retrieve and return all Users from the database.
exports.findAll = (req, res) => {
    UserData.find()
        .then(usersData => {
            res.send(usersData);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving usersData."
            });
        });
};

// Find a single UserData entry with ethereum Address (unique)
exports.findOne = (req, res) => {
    UserData.findOne({
            'ethereumAccountAddress': req.params.ethereumAccountAddress
        })
        .then(userData => {
            if (!userData) {
                return res.status(404).send({
                    message: "UserData not found with username " + req.params.ethereumAccountAddress
                });
            }
            res.send(userData);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "UserData not found with ethereum address " + req.params.ethereumAccountAddress
                });
            }
            return res.status(500).send({
                message: "Error retrieving user with username " + req.params.username
            });
        });
};