const Files = artifacts.require("Files");
const assert = require("assert");
let {
  sha3_512
} = require("js-sha3");

const filesNotFromBlockchain = [{
  name: "Master's Degree Sample",
  extension: ".pdf",
  size: 345435435,
  hash: sha3_512("Master's Degree Sample hash"),
  timestamp: (+new Date()).toString(),
  ethereumAddress: "0x0000000000000000000000000000000000000000",
  // metadata: "",
}];


contract("Files", function (accounts) {
  let filesContractInstance;
  beforeEach(async () => {
    filesContractInstance = await Files.deployed();
  });

  console.log("accounts: ", accounts);

  it("should be possible to store a file's properties in the Blockchain!"
    // And it should have a length of 1"
    , async () => {
      await filesContractInstance.addFile(
        filesNotFromBlockchain[0].name,
        filesNotFromBlockchain[0].hash,
        filesNotFromBlockchain[0].extension,
        filesNotFromBlockchain[0].size,
        filesNotFromBlockchain[0].timestamp,
        filesNotFromBlockchain[0].ethereumAddress, {
          from: accounts[0]
        }
      );

      let fileRetrievedFromSmartContract = await filesContractInstance.getFileAtIndex(
        0
      );


      const fileFromBlockchain = {
        name: fileRetrievedFromSmartContract[0],
        extension: fileRetrievedFromSmartContract[1],
        size: fileRetrievedFromSmartContract[2].toNumber(),
        hash: fileRetrievedFromSmartContract[3],
        timestamp: fileRetrievedFromSmartContract[4],
        ethereumAddress: fileRetrievedFromSmartContract[5],
      };

      assert.equal(
        JSON.stringify(filesNotFromBlockchain[0]),
        JSON.stringify(fileFromBlockchain),
        "File was not added correctly to the Blockchain :("
      );

      const filesNotFromBlockchainLength = filesNotFromBlockchain.length.toString();
      const fileFromBlockchainLength = await filesContractInstance.getUserFilesLength();
      assert.equal(
        filesNotFromBlockchainLength,
        fileFromBlockchainLength.toString(),
        "Something is wrong, there should be ONLY one added file in the Blockchain :("
      );
    });

  
  it("should NOT be possible to retrieve nonexistent files from the Blockchain!!!", async () => {
    const invalidFileIndex = 99;

    await filesContractInstance.addFile(
      filesNotFromBlockchain[0].name,
      filesNotFromBlockchain[0].hash,
      filesNotFromBlockchain[0].extension,
      filesNotFromBlockchain[0].size,
      filesNotFromBlockchain[0].timestamp,
      filesNotFromBlockchain[0].ethereumAddress, {
        from: accounts[0]
      }
    );

    try {
      await filesContractInstance.getFileAtIndex(invalidFileIndex);
      assert.ok(true, `file  is valid. `);
    } catch (error) {
     
      assert.equal(
       
        error,
        `Error: VM Exception while processing transaction: invalid opcode`
      );
    }
  });

 
});

