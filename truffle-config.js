var HDWalletProvider = require("truffle-hdwallet-provider");
// TODO: Configure Mocha

const mnemonicLocalhost = process.env.mnemonicLocalhost;
const mnemonicInfura = process.env.mnemonicInfura;
const infuraAPIKey = process.env.infuraAPIKey;

module.exports = {
  networks: {
    development: {
      /* Ganache */
      host: "localhost",
      port: 7545,
      network_id: "*" // Match any network id
    },
    rinkebyInfura: {
      provider: function () {
        return new HDWalletProvider(
          process.env.mnemonicInfura,
          'https://rinkeby.infura.io/' + infuraAPIKey
        )
      },
      network_id: 4
      // // remove these if they cause issues:
      // gas: 6612388, // Gas limit used for deploys
      // // from: "0xaa6C153202Ac204dAE3C62a3E5Bc0b851F10886D",
      // gasPrice: 20000000000, // 20 gwei
    }
  }

  // rpc: {
  //   host: "localhost",
  //   port: 8545
  // }
};

//https://ethereum.stackexchange.com/questions/28658/deploy-smart-contract-to-mainnet-with-infura-and-metamask/28660