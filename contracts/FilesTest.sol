pragma solidity 0.5.8;

import "../contracts/zeppelin/ownership/Ownable.sol";


/// @title Files
/// @author miniclip22
/// @notice This smart contract stores all the data extracted from a user uploaded file to IPFS. This includes: name, extension, size; IPFS hash and a timestamp (when the file was uploaded)

contract FilesTest is Ownable {

    // contract creator variable
    address contractCreator;

    // A structure that contains information about the user's files
    struct File {
        string name; // the file's name
        string extension; // the file's extension
        uint32 size; // the file's size
        string hash; //the IPFS file hash
        string timestamp; // the transaction timestamp is stored in the JavaScript and is in the Unix Epoch time;
        address ethereumAccount; // the user's Ethereum Address that uploaded the file
    }

    //The files belonging to each user (or address)
    mapping(address => File[]) private userFiles;

    /// @notice Contract constructor. Here, the contract creator variable is assigned the msg.sender value.
    constructor() public
    {
        contractCreator = msg.sender;
    }

    /// @notice This function inserts information of a file to the userFiles array: name, size, extension in addition to a timestamp and the IPFS file hash
    /// @param _name file name
    /// @param _hash IPFS file hash
    /// @param _extension file extension
    /// @param _size file size
    /// @param _timestamp a timestamp
    /// @param _ethereumAccount the user Ethereum account calling this contract
    function addFile(string memory _name, string memory _hash, string memory _extension, uint32 _size, string memory _timestamp, address _ethereumAccount)
    public {

        require(_size>0 && _size<=4294967295, "Invalid file size");
        File memory file = File(_name, _extension,  _size, _hash,_timestamp, _ethereumAccount);
        userFiles[contractCreator].push(file);
    }

    /// @notice This function gets all properties from the userFiles array
    /// @param index loop array index
    /// @return The properties of a file: name, extension, size, IPFS file hash and a timestamp
    function getFileAtIndex(uint256 index) public view returns(string memory, string memory, uint32, string memory,  string memory, address)
    {
        File storage file = userFiles[contractCreator][index];
        return (file.name, file.extension, file.size, file.hash, file.timestamp, file.ethereumAccount);
    }

    /// @notice This function gets the length of the userFiles array, so it can be looped in the frontend. This way, we avoid having costly loops in Solidity code
    /// @return The length of the userFiles array
    function getUserFilesLength() public view returns (uint256)
    {
        return userFiles[contractCreator].length;
    }
}